
import 'package:edasuavida/data/DataResult.dart';

CallbackRequestWrapper<T> wrapRequest<T>() => CallbackRequestWrapper<T>();

class CallbackRequestWrapper<T> {

  Function(T) _success;
  Function(Exception) _error;
  Function() _loading;

  CallbackRequestWrapper<T> onLoading(onLoading()) {
    _loading = onLoading;
    return this;
  }

  CallbackRequestWrapper<T> onError(onError(Exception error)) {
    _error = onError;
    return this;
  }

  CallbackRequestWrapper<T> onSuccess(onSuccess(T data)) {
    _success = onSuccess;
    return this;
  }

  void run(Function(Function(DataResult<T> result)) requestBlock) {
    requestBlock(_handleResult);
  }

  void _handleResult(DataResult<T> result) {
    switch (result.status) {
      case DataResultStatus.LOADING:
        if (_loading != null) {
          _loading();
        }
        break;
      case DataResultStatus.ERROR:
        if (_error != null && result.error != null) {
          _error(result.error);
        }
        break;
      case DataResultStatus.SUCCESS:
        if (_success != null && result.data != null) {
          _success(result.data);
        }
        break;
    }
  }
}
