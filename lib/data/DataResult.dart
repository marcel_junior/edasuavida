import 'package:flutter/material.dart';

enum DataResultStatus { LOADING, SUCCESS, ERROR }

class DataResult<T> {
  final T data;
  final Exception error;
  final DataResultStatus status;

  DataResult({this.data, this.error, @required this.status});

  static DataResult<T> withLoading<T>() =>
      DataResult(status: DataResultStatus.LOADING);

  static DataResult<T> withError<T>(Exception error) =>
      DataResult(status: DataResultStatus.ERROR, error: error);

  static DataResult<T> withSuccess<T>(T data) =>
      DataResult<T>(status: DataResultStatus.SUCCESS, data: data);
}
