import 'dart:convert';

import 'package:edasuavida/data/model/User.dart';
import 'package:shared_preferences/shared_preferences.dart';

const KEY_USER = "KEY_USER";
const KEY_ONBOARDING = "KEY_ONBOARDING";
const KEY_COVID_MESSAGE = "KEY_COVID_MESSAGE";

class ProjectDatabase {
  static final Future<SharedPreferences> _prefs =
      SharedPreferences.getInstance();

  static Future<void> burnOnBoarding() async {
    var preferences = await _prefs;
    preferences.setBool(KEY_ONBOARDING, true);
  }

  static Future<bool> isOnBoardingBurned() async {
    var preferences = await _prefs;
    return preferences.containsKey(KEY_ONBOARDING);
  }

  static Future<void> burnCOVIDMessage() async {
    var preferences = await _prefs;
    preferences.setBool(KEY_COVID_MESSAGE, true);
  }

  static Future<bool> isCOVIDMessageBurned() async {
    var preferences = await _prefs;
    return preferences.containsKey(KEY_COVID_MESSAGE);
  }

  static saveUser(User user) => _saveLocal(_prefs, KEY_USER, user);

  static getUser(callback(User result)) =>
      _getLocal<User>(_prefs, KEY_USER, (map) => User.fromJson(map), callback);

  static deleteUser() => saveUser(null);
}

void _saveLocal(Future<SharedPreferences> pref, String key, dynamic object) {
  pref.then((preferences) {
    try {
      if (object == null) {
        if (preferences.containsKey(key)) {
          preferences.remove(key);
        }
        return;
      }
      preferences.setString(key, jsonEncode(object));
    } catch (error) {
      // Something wrong happened... your object can be parsed as a JSON?
    }
  });
}

void _getLocal<T>(Future<SharedPreferences> pref, String key,
    T Function(Map map) parser, Function(T) callback) {
  pref.then((preferences) {
    try {
      var objectAsString = preferences.getString(key);
      if (objectAsString == null || objectAsString.isEmpty) {
        callback(null);
        return;
      }
      Map objectMap = jsonDecode(objectAsString);
      callback(parser(objectMap));
    } catch (error) {
      callback(null);
    }
  });
}
