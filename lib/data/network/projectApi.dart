import 'dart:convert';
import 'dart:io';

import 'package:edasuavida/constant/Constants.dart';
import 'package:edasuavida/data/DataResult.dart';
import 'package:edasuavida/data/model/Configs.dart';
import 'package:edasuavida/data/model/Contact.dart';
import 'package:edasuavida/data/model/ProposalResult.dart';
import 'package:edasuavida/data/model/User.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class ProjectApi {
  String _firebaseToken = '';

  static final ProjectApi _instance = ProjectApi._internal();

  ProjectApi._internal();

  factory ProjectApi() {
    return _instance;
  }

  static ProjectApi get instance => _instance;

  getContactsToShareList(
      String proposalId, callback(DataResult<List<Contact>> result)) {
    var execute = () => http.get(
        Constants.api_get_contacts_to_share.replaceAll('{id}', proposalId),
        headers: getDefaultHeaders());
    _wrapRequest(callback, execute, (result) {
      return (json.decode(result) as List)
          .map((i) => Contact.fromJson(i))
          .toList();
    });
  }

  getConfigs(callback(DataResult<Configs> result)) {
    var execute = () =>
        http.get(Constants.api_get_url_form, headers: getDefaultHeaders());

    _wrapRequest(callback, execute, (result) {
      return Configs.fromJson(json.decode(result));
    });
  }

  Future<ProposalResult> getCampaignList(int page, int pageSize,
      {String searchQuery, List<String> filterTypeList}) async {
    final request = Request('POST', Uri.parse(Constants.api_get_campaigns));
    request.headers["Content-Type"] = "application/json";

    var body = '"page_number": $page, "page_size": $pageSize';
    if (searchQuery != null && searchQuery.isNotEmpty) {
      body += ', "description": "$searchQuery"';
    }

    if (filterTypeList != null && filterTypeList.isNotEmpty) {
      String items = json.encode(filterTypeList);
      body += ', "tags": $items';
    }

    request.body = "{ $body }";

    var stream = (await request.send()).stream.transform(utf8.decoder);
    var response = await stream.join();

    return ProposalResult.fromJson(json.decode(response));
  }

  Future<List<String>> getTagsList(
      callback(DataResult<List<String>> result)) async {
    var execute = () =>
        http.get(Constants.api_get_campaigns, headers: getDefaultHeaders());
    execute =
        () => http.get(Constants.api_get_tags, headers: getDefaultHeaders());

    _wrapRequest(callback, execute, (result) {
      return (json.decode(result) as List<dynamic>).cast<String>();
    });
  }

  saveUser(callback(DataResult<String> user)) {
    var execute = () => http.post(Constants.api_get_user_url,
        headers: getDefaultHeaders(), body: json.encode({}));
    _wrapRequest(callback, execute, (result) {
      return result.toString();
    });
  }

  getUser(callback(DataResult<User> user)) {
    var execute = () =>
        http.get(Constants.api_get_user_url, headers: getDefaultHeaders());
    _wrapRequest(callback, execute, (result) {
      return User.fromJson(json.decode(result));
    });
  }

  getDefaultHeaders() => {
        HttpHeaders.authorizationHeader:
            _firebaseToken == null ? '' : '' "Bearer " + _firebaseToken,
        HttpHeaders.acceptHeader: 'application/json; charset=utf-8',
        HttpHeaders.contentTypeHeader: 'application/json; charset=utf-8'
      };

  setToken(String firebaseToken) {
    this._firebaseToken = firebaseToken;
  }
}

void _wrapRequest<T>(callback(DataResult<T> result),
    Future<http.Response> Function() execute, T Function(String json) mapper) {
  callback(DataResult.withLoading());

  execute().then((result) {
    try {
      callback(DataResult.withSuccess(mapper(utf8.decode(result.bodyBytes))));
    } catch (error) {
      if (error is Exception) {
        callback(DataResult.withError(error));
      } else {
        callback(DataResult.withError(FormatException(error.toString())));
      }
    }
  }, onError: (error) {
    callback(DataResult.withError(error));
  });
}
