class ChooseAWayItem {
  String image;
  String title;
  String description;
  String actionRoute;
  String flag;
  bool enabled;

  ChooseAWayItem(this.image, this.title, this.description, this.actionRoute, this.flag, this.enabled);
}
