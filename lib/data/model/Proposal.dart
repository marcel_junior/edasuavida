
//{
//"created_at": "2020-04-19T13:16:33Z",
//"data_to_share": [],
//"description": "string",
//"estimated_value": 0,
//"expose_user_data": true,
//"images": [],
//"is_active": true,
//"last_update": "2020-04-19T13:16:33Z",
//"proposal_id": "string",
//"proposal_type": "job",
//"proposal_validate": "2020-04-19T13:16:33Z",
//"side": "offer",
//"tags": [],
//"target_area": {},
//"title": "string",
//"user_id": "string"
//}

import 'package:edasuavida/data/model/TargetArea.dart';
import 'package:json_annotation/json_annotation.dart';

part 'Proposal.g.dart';

@JsonSerializable()
class Proposal {

  @JsonKey(name: "created_at")
  final String createdAt;
  @JsonKey(name: "data_to_share")
  final List<String> dataToShare;
  @JsonKey(name: "estimated_value")
  final double estimatedValue;
  @JsonKey(name: "expose_user_data")
  final bool exposeUserData;
  final List<String> images;
  @JsonKey(name: "is_active")
  final bool isActive;
  @JsonKey(name: "last_update")
  final String lastUpdate;
  @JsonKey(name: "proposal_id")
  final String proposalId;
  @JsonKey(name: "proposal_type")
  final String proposalType;
  @JsonKey(name: "proposal_validate")
  final String proposalValidate;
  final String side;
  final List<String> tags;
  @JsonKey(name: "target_area")
  final TargetArea targetArea;
  final String title;
  final String description;
  /*local variable*/
  String imageURL;

  Proposal(this.createdAt, this.dataToShare, this.estimatedValue,
      this.exposeUserData, this.images, this.isActive, this.lastUpdate,
      this.proposalId, this.proposalType, this.proposalValidate, this.side,
      this.tags, this.targetArea, this.title, this.description);


  factory Proposal.fromJson(json) => _$ProposalFromJson(json);

  Map<String, dynamic> toJson() => _$ProposalToJson(this);

}
