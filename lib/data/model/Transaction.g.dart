// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Transaction.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Transaction _$TransactionFromJson(Map<String, dynamic> json) {
  return Transaction(
    json['created_at'] as String,
    json['giver_id'] as String,
    json['giver_review'] == null
        ? null
        : GiverReview.fromJson(json['giver_review']),
    json['last_update'] as String,
    json['proposal_id'] as String,
    json['status'] as String,
    json['taker_id'] as String,
    json['taker_review'] == null
        ? null
        : TakerReview.fromJson(json['taker_review']),
    json['transaction_id'] as String,
  );
}

Map<String, dynamic> _$TransactionToJson(Transaction instance) =>
    <String, dynamic>{
      'created_at': instance.createdAt,
      'giver_id': instance.giverId,
      'giver_review': instance.giverReview,
      'last_update': instance.lastUpdate,
      'proposal_id': instance.proposalId,
      'status': instance.status,
      'taker_id': instance.takerId,
      'taker_review': instance.takerReview,
      'transaction_id': instance.transactionId,
    };
