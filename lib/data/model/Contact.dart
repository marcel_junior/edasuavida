


import 'package:json_annotation/json_annotation.dart';

part 'Contact.g.dart';

@JsonSerializable()
class Contact {

  String contact;
  @JsonKey(name: 'contact_type')
  String contactType;

  Contact(this.contact, this.contactType);

  factory Contact.fromJson(json) => _$ContactFromJson(json);

  Map<String, dynamic> toJson() => _$ContactToJson(this);

}