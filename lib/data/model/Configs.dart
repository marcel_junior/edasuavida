import 'package:json_annotation/json_annotation.dart';

part 'Configs.g.dart';

@JsonSerializable()
class Configs {
  @JsonKey(name: 'default_url')
  String defaultUrl;
  @JsonKey(name: 'ask_help_url')
  String askHelpUrl;

  Configs();

  factory Configs.fromJson(json) => _$ConfigsFromJson(json);

  Map<String, dynamic> toJson() => _$ConfigsToJson(this);
}
