
import 'package:json_annotation/json_annotation.dart';

part 'TargetArea.g.dart';

@JsonSerializable()
class TargetArea {

  @JsonKey(name: "area_tags")
  final List<String> areaTags;
  @JsonKey(name: "lat")
  final double latitude;
  @JsonKey(name: "long")
  final double longitude;
  final double range;

  TargetArea(this.areaTags, this.latitude, this.longitude, this.range);

  factory TargetArea.fromJson(json) => _$TargetAreaFromJson(json);

  Map<String, dynamic> toJson() => _$TargetAreaToJson(this);

}