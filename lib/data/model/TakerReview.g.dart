// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'TakerReview.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TakerReview _$TakerReviewFromJson(Map<String, dynamic> json) {
  return TakerReview(
    json['comment'] as String,
    (json['rating'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$TakerReviewToJson(TakerReview instance) =>
    <String, dynamic>{
      'comment': instance.comment,
      'rating': instance.rating,
    };
