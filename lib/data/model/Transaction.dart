

import 'package:edasuavida/data/model/GiverReview.dart';
import 'package:edasuavida/data/model/TakerReview.dart';
import 'package:json_annotation/json_annotation.dart';

part 'Transaction.g.dart';

@JsonSerializable()
class Transaction {

  @JsonKey(name: 'created_at')
  final String createdAt;
  @JsonKey(name: 'giver_id')
  final String giverId;
  @JsonKey(name: 'giver_review')
  final GiverReview giverReview;
  @JsonKey(name: "last_update")
  final String lastUpdate;
  @JsonKey(name: "proposal_id")
  final String proposalId;
  final String status;
  @JsonKey(name: "taker_id")
  final String takerId;
  @JsonKey(name: "taker_review")
  final TakerReview takerReview;
  @JsonKey(name: "transaction_id")
  final String transactionId;

  Transaction(this.createdAt, this.giverId, this.giverReview, this.lastUpdate,
      this.proposalId, this.status, this.takerId, this.takerReview,
      this.transactionId);

  factory Transaction.fromJson(json) => _$TransactionFromJson(json);

  Map<String, dynamic> toJson() => _$TransactionToJson(this);

}