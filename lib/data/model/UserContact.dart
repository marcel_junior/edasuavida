import 'package:json_annotation/json_annotation.dart';

part 'UserContact.g.dart';

@JsonSerializable()
class UserContact {

  String email;

  UserContact({this.email});

  factory UserContact.fromJson(json) => _$UserContactFromJson(json);

  Map<String, dynamic> toJson() => _$UserContactToJson(this);
}
