// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ProposalResult.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProposalResult _$ProposalResultFromJson(Map<String, dynamic> json) {
  return ProposalResult(
    json['current_page'] as int,
    json['current_page_size'] as int,
    (json['result'] as List)
        ?.map((e) => e == null ? null : Proposal.fromJson(e))
        ?.toList(),
  );
}

Map<String, dynamic> _$ProposalResultToJson(ProposalResult instance) =>
    <String, dynamic>{
      'current_page': instance.currentPage,
      'current_page_size': instance.currentPageSize,
      'result': instance.result,
    };
