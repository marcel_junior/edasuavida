// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'TargetArea.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TargetArea _$TargetAreaFromJson(Map<String, dynamic> json) {
  return TargetArea(
    (json['area_tags'] as List)?.map((e) => e as String)?.toList(),
    (json['lat'] as num)?.toDouble(),
    (json['long'] as num)?.toDouble(),
    (json['range'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$TargetAreaToJson(TargetArea instance) =>
    <String, dynamic>{
      'area_tags': instance.areaTags,
      'lat': instance.latitude,
      'long': instance.longitude,
      'range': instance.range,
    };
