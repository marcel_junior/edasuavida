
import 'package:json_annotation/json_annotation.dart';

part 'GiverReview.g.dart';

@JsonSerializable()
class GiverReview {

  final String comment;
  final double rating;

  GiverReview(this.comment, this.rating);


  factory GiverReview.fromJson(json) => _$GiverReviewFromJson(json);

  Map<String, dynamic> toJson() => _$GiverReviewToJson(this);

}