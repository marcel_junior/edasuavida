import 'package:json_annotation/json_annotation.dart';

import 'UserContact.dart';

part 'User.g.dart';


@JsonSerializable()
class User {

  UserContact contact;
  @JsonKey(name: 'created_at')
  String createdAt;
  @JsonKey(name: 'last_update')
  String lastUpdate;
  String name;
  @JsonKey(name: 'register_from')
  String registerFrom;
  @JsonKey(name: 'user_id')
  String userId;
  bool skipTutorial;

  User({this.contact, this.createdAt, this.lastUpdate, this.name,
      this.registerFrom, this.userId});

  factory User.fromJson(json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);


}
