
import 'package:json_annotation/json_annotation.dart';

part 'TakerReview.g.dart';

@JsonSerializable()
class TakerReview {

  final String comment;
  final double rating;

  TakerReview(this.comment, this.rating);

  factory TakerReview.fromJson(json) => _$TakerReviewFromJson(json);

  Map<String, dynamic> toJson() => _$TakerReviewToJson(this);

}