// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Proposal.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Proposal _$ProposalFromJson(Map<String, dynamic> json) {
  return Proposal(
    json['created_at'] as String,
    (json['data_to_share'] as List)?.map((e) => e as String)?.toList(),
    (json['estimated_value'] as num)?.toDouble(),
    json['expose_user_data'] as bool,
    (json['images'] as List)?.map((e) => e as String)?.toList(),
    json['is_active'] as bool,
    json['last_update'] as String,
    json['proposal_id'] as String,
    json['proposal_type'] as String,
    json['proposal_validate'] as String,
    json['side'] as String,
    (json['tags'] as List)?.map((e) => e as String)?.toList(),
    json['target_area'] == null
        ? null
        : TargetArea.fromJson(json['target_area']),
    json['title'] as String,
    json['description'] as String,
  )..imageURL = json['imageURL'] as String;
}

Map<String, dynamic> _$ProposalToJson(Proposal instance) => <String, dynamic>{
      'created_at': instance.createdAt,
      'data_to_share': instance.dataToShare,
      'estimated_value': instance.estimatedValue,
      'expose_user_data': instance.exposeUserData,
      'images': instance.images,
      'is_active': instance.isActive,
      'last_update': instance.lastUpdate,
      'proposal_id': instance.proposalId,
      'proposal_type': instance.proposalType,
      'proposal_validate': instance.proposalValidate,
      'side': instance.side,
      'tags': instance.tags,
      'target_area': instance.targetArea,
      'title': instance.title,
      'description': instance.description,
      'imageURL': instance.imageURL,
    };
