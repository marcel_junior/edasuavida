// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'User.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    contact:
        json['contact'] == null ? null : UserContact.fromJson(json['contact']),
    createdAt: json['created_at'] as String,
    lastUpdate: json['last_update'] as String,
    name: json['name'] as String,
    registerFrom: json['register_from'] as String,
    userId: json['user_id'] as String,
  )..skipTutorial = json['skipTutorial'] as bool;
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'contact': instance.contact,
      'created_at': instance.createdAt,
      'last_update': instance.lastUpdate,
      'name': instance.name,
      'register_from': instance.registerFrom,
      'user_id': instance.userId,
      'skipTutorial': instance.skipTutorial,
    };
