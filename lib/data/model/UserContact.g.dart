// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'UserContact.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserContact _$UserContactFromJson(Map<String, dynamic> json) {
  return UserContact(
    email: json['email'] as String,
  );
}

Map<String, dynamic> _$UserContactToJson(UserContact instance) =>
    <String, dynamic>{
      'email': instance.email,
    };
