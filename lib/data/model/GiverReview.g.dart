// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'GiverReview.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GiverReview _$GiverReviewFromJson(Map<String, dynamic> json) {
  return GiverReview(
    json['comment'] as String,
    (json['rating'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$GiverReviewToJson(GiverReview instance) =>
    <String, dynamic>{
      'comment': instance.comment,
      'rating': instance.rating,
    };
