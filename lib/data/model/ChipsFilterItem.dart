class ChipsFilterItem {

  final String text;
  bool isSelected;

  ChipsFilterItem(this.text, {this.isSelected = false});

}
