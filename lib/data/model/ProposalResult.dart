


import 'package:edasuavida/data/model/Proposal.dart';
import 'package:json_annotation/json_annotation.dart';

part 'ProposalResult.g.dart';

@JsonSerializable()
class ProposalResult {

  @JsonKey(name: "current_page")
  final int currentPage;
  @JsonKey(name: "current_page_size")
  final int currentPageSize;
  final List<Proposal> result;

  ProposalResult(this.currentPage, this.currentPageSize, this.result);

  factory ProposalResult.fromJson(json) => _$ProposalResultFromJson(json);

  Map<String, dynamic> toJson() => _$ProposalResultToJson(this);

}