// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Configs.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Configs _$ConfigsFromJson(Map<String, dynamic> json) {
  return Configs()
    ..defaultUrl = json['default_url'] as String
    ..askHelpUrl = json['ask_help_url'] as String;
}

Map<String, dynamic> _$ConfigsToJson(Configs instance) => <String, dynamic>{
      'default_url': instance.defaultUrl,
      'ask_help_url': instance.askHelpUrl,
    };
