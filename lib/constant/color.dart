import 'package:flutter/material.dart';

class ProjectColors {
  ProjectColors._();

  static const Color primary_1 = Color(0xFFFF6060);
  static const Color primary_2 = Color(0xFFFF7D7D);
  static const Color primary_3 = Color(0xFFFF6060);
  static const Color primary_4 = Color(0xFFFFBFBF);
  static const Color primary_5 = Color(0xFFFFDFDF);
  static const Color primary_6 = Color(0xFFFFF0F0);
  static final MaterialColor primary = MaterialColor(0xFFFF6060, <int, Color>{
    50: primary_6,
    100: primary_5,
    200: primary_4,
    300: primary_3,
    400: primary_2,
    500: primary_1,
    600: primary_1,
    700: primary_1,
    800: primary_1,
    900: primary_1,
  });

  static const Color secondary_1 = Color(0xFF2C364D);
  static const Color secondary_2 = Color(0xFF565E70);
  static const Color secondary_3 = Color(0xFF818694);
  static const Color secondary_4 = Color(0xFFABAFB8);
  static const Color secondary_5 = Color(0xFFD5D7DB);
  static const Color secondary_7 = Color(0xFFF5F5F5);
  static const Color secondary_6 = Color(0xFFEAEBEE);

  static const Color highlight_1 = Color(0x77EAEBEE);
  static const Color highlight_2 = Color(0xFF437596);
  static const Color highlight_3 = Color(0xB22C364D);

  static const Color disabled_1 = Color(0xFFFAFBFC);

  static const Color shadow_1 = Color(0x40000000);
  static const Color shadow_2 = Color(0xAA000000);

  static const Color background_1 = Color(0xFFFCFDFF);
  static const Color background_2 = Color(0xA3000000);

  static final MaterialColor secondary = MaterialColor(0xFF2C364D, <int, Color>{
    30: secondary_7,
    50: secondary_6,
    100: secondary_5,
    200: secondary_4,
    300: secondary_3,
    400: secondary_2,
    500: secondary_1,
    600: secondary_1,
    700: secondary_1,
    800: secondary_1,
    900: secondary_1,
  });

  static const Gradient gradient_1 = LinearGradient(
      begin: Alignment(-1.0, -4.0),
      end: Alignment(1.0, 4.0),
      colors: [Color(0xFFFF6096), Color(0xFFFF6060)]);

  static const Color gray_1 = Color(0xFFEEEEEE);
  static const Color gray_2 = Color(0xFFC4C4C4);

  static const Color success = Color(0xFF00B295);
  static const Color error = Color(0xFFCC0000);
}
