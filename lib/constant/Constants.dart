class sConstants {
  //Api Routes
  static const String api_url = 'https://api.todosunidos.com.vc/v1';
  static const String api_get_user_url = '$api_url/user';
  static const String api_get_campaigns = '$api_url/proposal';
  static const String api_get_tags = '$api_url/tags';
  static const String api_get_filtered_campaigns =
      '$api_url/proposal?query={query}';
  static const String api_get_contacts_to_share =
      '$api_url/proposal/{id}/share';
  static const String api_get_url_form = '$api_url/etc';

  //Emails
  static const String contact_email = 'precisodeajuda@todosunidos.app';
  static const String report_email = 'denuncie@todosunidos.app';

  //Routes
  static const String view_intro = '/IntroView';
  static const String view_choose_a_way = '/ChooseAwayView';
  static const String view_login = '/LoginView';
  static const String view_campaign_list = '/CampaignListView';
  static const String view_campaign_detail = '/CampaignDetailView';
  static const String view_home = '/Home';
  static const String view_sign_up_user = '/SignUpUserView';
  static const String view_create_password = '/CreatePasswordView';
  static const String view_sign_up_campaign = '/SignUpCampaignView';
  static const String view_contact = '/ContactView';

  //Images
  static const String asset_image_path = 'assets/images';
  static const String image_offer_help = '$asset_image_path/ic_offer_help.png';
  static const String image_search_help =
      '$asset_image_path/ic_search_help.png';
  static const String image_back_button =
      '$asset_image_path/image_back_button.png';
  static const String image_star = '$asset_image_path/image_star.png';
  static const String image_facebook = '$asset_image_path/image_facebook.png';
  static const String image_google = '$asset_image_path/image_google.png';
  static const String image_instagram = '$asset_image_path/image_instagram.png';
  static const String intro_image_1 = '$asset_image_path/intro_image1.png';
  static const String intro_image_2 = '$asset_image_path/intro_image2.png';
  static const String intro_image_3 = '$asset_image_path/intro_image3.png';
  static const String icon_password_hide =
      '$asset_image_path/icon_password_hide.png';
  static const String icon_arrow_down = '$asset_image_path/arrow_down.png';
  static const String icon_share = '$asset_image_path/icon_share.png';
  static const String icon_heart = '$asset_image_path/icon_heart.png';
  static const String icon_flag = '$asset_image_path/icon_flag.png';
  static const String icon_search = '$asset_image_path/icon_search.png';
  static const String icon_close = '$asset_image_path/icon_close.png';
  static const String icon_heart_2 = '$asset_image_path/icon_heart_2.png';
  static const String icon_filter = '$asset_image_path/icon_filter.png';
  static const String icon_flag_2 = '$asset_image_path/icon_flag_2.png';
  static const String icon_arrow_right =
      '$asset_image_path/icon_arrow_right.png';
  static const String icon_report = '$asset_image_path/icon_report.png';
  static const String icon_type_filter =
      '$asset_image_path/icon_type_filter.png';
  static const String icon_type_filter_selected =
      '$asset_image_path/icon_type_filter_selected.png';

  //Lottie
  static const String lottie_empty = 'assets/lottie/lottie_empty.json';
  static const String lottie_error = 'assets/lottie/lottie_error.json';
  static const String lottie_progress_image =
      'assets/lottie/lottie_progress_image.json';
  static const String lottie_error_image =
      'assets/lottie/lottie_image_error.json';

  //Strings
  static const String app_name = 'TodosUnidos';
  static const String what_you_want_to_do = 'O que você pode fazer:';
  static const String offer_help = 'Oferecer ajuda';
  static const String i_want_to_help_description =
      'Veja como você pode ajudar outras pessoas e instituições';
  static const String i_need_help_title = 'Procurar ajuda';
  static const String i_need_help_description =
      'Fale o que você precisa e como podemos ajudar';
  static const String i_want_to_help_flag = 'Logo mais';
  static const String enter = 'Entrar';
  static const String you_need_do_login_to_enter =
      'Você precisa fazer login para continuar.';
  static const String enter_with_google_account = 'Entrar com uma conta Google';
  static const String enter_with_facebook = 'Entrar com o Facebook';
  static const String enter_with_instagram = 'Entrar com o Instagram';
  static const String or = 'ou';
  static const String enter_in_another_way = 'Entrar de outra maneira';
  static const String i_agree_with_the = 'Estou de acordo com os ';
  static const String use_terms = 'Termos de Uso';
  static const String and = ' e ';
  static const String privacy_policy = 'Políticas de Privacidade';
  static const String sign_up = 'Cadastro';
  static const String fill_fields_below_to_continue =
      'Preencha os campos abaixo para continuar';
  static const String complete_name = 'Nome completo';
  static const String phone = 'Telefone';
  static const String email = 'E-mail';
  static const String next = 'Próximo';
  static const String done = 'Vamos lá';
  static const String intro_title_1 = 'Conheça as formas de contribuir';
  static const String intro_description_1 =
      'Pesquise os projetos que mais têm a ver com você';
  static const String intro_title_2 = 'Encontre quem ajudar';
  static const String intro_description_2 =
      'Fale diretamente com quem você quer apoiar';
  static const String intro_title_3 = 'Faça sua contribuição';
  static const String intro_description_3 =
      'Crie uma rede de apoio direta e simples com quem mais precisa';
  static const String create_a_password_to_enter_in_app =
      'Crie uma senha para entrar no app';
  static const String sign_up_campaign = 'Cadastrar campanha';
  static const String type = 'Tipo';
  static const String category = 'Categoria';
  static const String site = 'Site';
  static const String description = 'Descrição';
  static const String how_to_help_Title = 'Pronto!';
  static const String how_to_help_description =
      'Os dados do projeto que você escolheu são:';
  static const String search_a_campaign = 'Procure um projeto';
  static const String filter = 'Filtro';
  static const String order = 'Ordenar';
  static const String how_you_can_help_title = 'Está na hora de ajudar';
  static const String how_you_can_help_description =
      'Encontre o projeto que mais tem a ver com você e contribua';
  static const String got_it = 'Entendi';
  static const String covid_dialog_title = 'Juntos contra o coronavírus';
  static const String covid_dialog_description =
      'Siga as orientações da OMS, do Ministério da Saúde e do governo local na prevenção para evitar a Covid-19.';
  static const String help_type = 'Tipo de ajuda';
  static const String apply_filter = 'Aplicar Filtro';

  static const String share_message_1 = 'Dados para ajudar em';
  static const String share_message_2 =
      'Obrigado por ajudar! Compartilhe o app com os amigos e fortaleça essa corrente de ajuda.\n\nhttps://todosunidos.com.vc';
  static const String share_message_detail =
      'Você já conhece o projeto {name}? Que tal contribuir: https://todosunidos.com.vc';

  static const String campaign_list_empty_title = 'Está na hora de ajudar!';
  static const String campaign_list_empty_subtitle =
      'Procure um projeto e apoie quem precisa!';
  static const String campaign_list_empty_action = 'Tentar novamente';
  static const String campaign_list_error_title = 'Ops!';
  static const String campaign_list_error_subtitle =
      'Não conseguimos encontrar resultados para sua pesquisa.';
  static const String campaign_list_error_action = 'Tentar novamente';
  static const String campaign_list_error_list_action =
      'Erro ao carregar o projeto. Tente novamente.';
  static const String campaign_list_end_pagination = 'Por hoje é só, pessoal';

  static const String campaign_search_title = 'Está na hora de ajudar!';
  static const String campaign_search_subtitle =
      'Procure um projeto e apoie quem precisa.';
  static const String campaign_search_error_size_title =
      'Não conseguimos encontrar resultados para sua pesquisa';
  static const String campaign_search_error_size_subtitle =
      'Sua busca precisa ter pelo menos 3 letras.';

  static const String campaign_detail_help = 'Ajudar agora';
  static const String campaign_detail_fo_to_site = 'Ir para o site >';

  static const String label_pf = 'Pessoa física';
  static const String label_pj = 'Pessoa Jurídica';
  static const String label_commerce = 'Comércio';

  static const String need_help_and_cant_wait =
      'Precisa de ajuda e não pode esperar?';
  static const String talk_our_team = 'Fale com a nossa equipe.';
  static const String i_need_help = 'Preciso de ajuda';

  static const String report_campaign = 'Fazer denúncia';
  static const String report_campaign_email_title = 'Fazer denúncia - {name}';

  //Errors
  static const String type_your_name = 'Digite seu Nome';
  static const String type_your_name_correctly = 'Digite seu Nome corretamente';
  static const String type_your_phone = 'Digite seu Telefone';
  static const String type_your_phone_correctly =
      'Digite seu Telefone corretamente';
  static const String type_your_email = 'Digite seu E-mail';
  static const String type_your_email_correctly =
      'Digite seu E-mail corretamente';
  static const String type_your_password = 'Você precisa criar uma Senha';
  static const String minimum_password_length = 'No mínimo {value} caracteres';
  static const String you_need_accept_use_terms =
      'Você precisa aceitar os termos de uso para continuar';

  //phoneMask
  static const String phone_mask = '(00) 00000-0000';

  //Terms
  static const String terms_text =
      'Vivemos em um mundo em que diversas restrições acabam impactando a vida dos diversos seres vivos que o habitam. Todos esses impactos nos fazem repensar a forma como enxergamos e conhecemos o mundo, e acabam nos levando ao que realmente importa: uns aos outros.\n\nSempre poderemos ajudar alguém. Devemos ajudar aos nossos, as pessoas que amamos, conhecemos e convivemos diariamente. Devemos ajudar as nossas comunidades. Para isto, aqueles que se dispõem a doar recursos, tempo e conhecimento devem encontrar aqueles que desejam recebe-los, sejam estes alimentação, ajuda, grana, apoio, dentre outros.\n\nA TodosUnidos é o lugar ideal para que todo aquele que deseja oferecer ajuda, encontre alguém que deseja recebe-la, e vice-versa. Por ajuda, não falamos necessariamente de filantropia; entendemos que comércios locais, alimentação em geral, artesões, autônomos fazem parte deste grupo. Famílias, idosos, pessoas em situação de rua, desempregados... o cadastro pode ser amplo, e realizado pelo próprio solicitante ou por outro que conhece um terceiro que esteja passando por necessidades.\n\nEstamos todos em situações de vulnerabilidade; ricos em alguns recursos, e pobres em outros. Por isso a TodosUnidos está aqui, a fim de fomentar a troca verdadeira entre membros na comunidade, e auxiliar a todos na superação de momentos tão complicados. Vamos em frente!\n\n#épranossavida\n\nPara que possa realizar seu cadastro e utilizar a plataforma TodosUnidos (“Plataforma”), disponibilizada e mantida pela Associação Civil Todos Unidos (doravante “TodosUnidos”), você (“Você”) deverá ler e aceitar os presentes Termos de Uso e Política de Privacidade (doravante “Termos”), que têm como escopo regular o acesso e o uso da Plataforma, bem como dispor sobre o tratamento de seus dados pessoais.\n\nVocê poderá verificar as disposições destes Termos a qualquer momento, por meio da Plataforma.\n\nAntes de utilizar a Plataforma, Você deverá ler atentamente todo o conteúdo deste documento, e, caso esteja de acordo, manifestar seu consentimento livre, expresso, informado, e inequívoco acerca de\n\ntodas as disposições aqui apresentadas, por meio do seu clique no botão “Cadastrar”. Tal consentimento poderá ser revogado a qualquer momento, por meio de um de nossos Canais de Atendimento indicados no item 13 destes Termos, ok? Entretanto, ao revogar o seu consentimento, Você compreende que isto poderá restringir ou suspender o seu acesso à Plataforma. De todo modo, assim que a TodosUnidos receber sua solicitação, seus dados pessoais serão excluídos, exceto caso o armazenamento destes dados decorra de obrigação legal.\n\n1. Como posso acessar à Plataforma?\n    A Plataforma está disponível para download, em smartphones e tablets, nas lojas virtuais App Store e Google Play, podendo ser também acessada via web.\n\n    Por meio da Plataforma, Você ainda poderá acessar nosso Catálogo de Pedidos de Ajuda (se doador) ou nosso Catálogo de Doações (se desejar receber ajuda).\n\n    Para tanto, será necessário que Você realize o seu cadastro na Plataforma, de acordo com as orientações indicadas no item 2, abaixo.\n\n2. Como cadastro uma oferta ou pedido de ajuda na Plataforma?\n    Ao aceitar estes Termos, Você compreende que não poderá, em hipótese alguma, cadastrar na Plataforma ofertas ou pedidos de ajuda que estejam relacionados a conteúdos ilícitos, imorais, inapropriados ou, de qualquer forma, em desacordo com a legislação vigente.\n\n    Ainda, Você também se declara ciente de que todo o processo de negociação ocorrerá em ambiente externo à Plataforma, ao qual a TodosUnidos não possui qualquer ingerência. Neste sentido, Você compreende que deverá adotar todas as precauções adequadas para verificação do destinatário da oferta ou do pedido, de modo a garantir sua segurança, não podendo ser a TodosUnidos ser responsabilizada por qualquer dano eventualmente causado a Você ou a terceiro em decorrências de tais negociações.\n\n    Caso Você tenha qualquer problema com algum usuário da Plataforma, ou deseje elogiá-lo, após a conclusão das negociações Você poderá avaliar como foi o processo estabelecido com este usuário, por meio da nossa Plataforma online.\n    \n    Para a realização de seu cadastro na Plataforma TodosUnidos, Você deverá seguir os seguintes passos:\n        a) Se Você quiser oferecer ajuda:\n            1) Inserir o seu Nome Completo ou Razão Social, se pessoa jurídica, e-mail, Telefone e Endereço;\n            2) Data de Nascimento;\n            3) Se pessoa jurídica, fornecer o nome completo da pessoa que será contato da empresa;\n            4) Indicar a sua oferta de ajuda; e\n            5) Publicar a sua oferta de ajuda.\n        b) Se Você quiser receber ajuda:    \n            Se Você deseja cadastrar um pedido de ajuda, deverá:\n                1) Fornecer seu Nome completo, E-mail e Endereço e Telefone;\n                2) Data de Nascimento;\n                3) Indicar a qual ajuda se refere o pedido; e\n                4) Publicar seu pedido de ajuda.\n        c) Se quiser cadastrar um pequeno negócio:\n            1) Fornecer sua Razão Social, Nome Fantasia, Endereço e Telefone;\n            2) Fornecer o Nome completo da pessoa que será contato do pequeno negócio;\n            3) Indicar a qual ajuda se refere o pedido; e\n            4) Publicar seu pedido de ajuda.\n\n    Você reconhece ser o único responsável pelo fornecimento de dados verídicos, corretos, exatos e atualizados para a realização do cadastro, eximindo a TodosUnidos de toda e qualquer responsabilidade relativa à eventuais informações falsas ou inexatas, bem como por eventuais danos e prejuízos decorrentes de tais informações causados a Você, a terceiros ou à própria TodosUnidos.    \n\n    Você compreende ainda que, caso queira realizar o cadastro de um terceiro, na Plataforma, para oferta ou pedido de ajuda, é de sua exclusiva responsabilidade obter a prévia e expressa autorização do titular dos dados.\n\n    Note que Você poderá optar por compartilhar seus dados pessoais automaticamente com o usuário que pretender receber ou oferecer ajuda, ou apenas após sua prévia autorização. Neste caso, Você poderá selecionar os dados que deseja compartilhar com o usuário solicitante, combinado?\n\n    Por fim, a partir do aceite aos presentes Termos, Você se declara ciente de ser o único e exclusivo responsável pelas informações fornecidas quando de seu acesso ao Aplicativo, responsabilizando-se perante a TodosUnidos e inclusive perante terceiros por danos ou prejuízos decorrentes do fornecimento de informações incorretas, incompletas ou inverídicas.\n\n3. Quais as regras para a utilização da Plataforma?\n    Para utilizar a Plataforma, você declara estar ciente das seguintes condições:\n        a) Ser maior de 18 anos e considerado civilmente capaz à luz da legislação brasileira. Caso Você não seja maior de 18 anos, deverá solicitar a seu representante legal que nos encaminhe a solicitação de cadastro por meio dos Canais de Atendimento indicados no item 13 destes Termos;\n        b) Aceitar estes Termos;\n        c) Ser responsável por quaisquer consequências relacionadas à sua utilização da Plataforma, respondendo, inclusive perante terceiros, por qualquer reivindicação que venha a ser apresentada à TodosUnidos, judicial ou extrajudicialmente;\n        d) Que não utilizará a Plataforma para quaisquer fins ilícitos, devendo ser, sob qualquer circunstância, observada a legislação vigente à época;\n        e) Reconhecer que lhe compete única e exclusivamente a garantia do fornecimento de dados cadastrais fidedignos;\n        f) Reconhecer que esta Plataforma não deverá ser utilizada para quaisquer fins ilícitos;\n        g) Estar ciente de que não poderá ser inserido qualquer conteúdo ou material capaz de incorporar elementos nocivos na Plataforma, por quaisquer meios e formas, capazes de impedir o seu normal funcionamento, bem como de prejudicar os sistemas informáticos da TodosUnidos ou de terceiros, de danificar documentos e/ou arquivos eletrônicos nestes armazenados;\n        h) Compreender que não deverá solicitar ou ofertar, por meio da Plataforma, quaisquer produtos e/ou serviços imorais, ilegais ou inapropriados, como, por exemplo, conteúdo pornográfico, serviços de cunho sexual, comercialização de entorpecentes, dentre outros;\n        i) Estar ciente de que não poderão ser ofertados produtos regulados, por meio da Plataforma, como por exemplo, mas não limitado a, medicamentos, dispositivos de telecomunicação, dentre outros, que não tenham sido devidamente homologados e certificados pela respectiva agência reguladora;\n        j) Compreender que suas credenciais de acesso, tais como, login e senha, são de natureza individual e intransferível, não devendo ser fornecidas a terceiros; e\n        k) Reconhecer que, em caso de suspeita de ocorrência de fraude, seu cadastro e acesso poderão ser suspensos ou bloqueados a qualquer tempo.\n\n    Ao aceitar estes Termos, Você se declara ciente de que, em qualquer hipótese, é o único responsável pela utilização da Plataforma e pela confidencialidade de suas credenciais de acesso, ou de terceiros os quais tenha cadastrado na Plataforma, isentando a TodosUnidos de toda e qualquer responsabilidade por danos e prejuízos decorrentes do uso indevido da Plataforma e de tais credenciais.\n\n4. A quem pertencem a propriedade intelectual da Plataforma?\n    Todos os materiais, incluindo textos, imagens, ilustrações, ícones, tecnologias, links e demais conteúdos audiovisuais ou sonoros, inclusive o software da Plataforma, desenhos gráficos e códigos-fonte, que façam referência à TodosUnidos, são de propriedade única e exclusiva da TodosUnidos ou de terceiro que tenha autorizado sua utilização na Plataforma, estando protegidos pelas leis e tratados internacionais, sendo absolutamente vedada sua cópia, reprodução, ou qualquer outro tipo de utilização, ficando os infratores sujeitos às sanções civis e criminais correspondentes, nos termos das Leis n. 9.279/96, 9.609/98, 9.610/98, de modo que a utilização da Plataforma não importa autorização para que Você possa citar as tais marcas, nomes comerciais e logotipos.\n\n    Caso Você identifique a violação de direitos relativos à propriedade intelectual, poderá enviar uma denúncia por meio do Canal de Denúncia indicado no item 11 destes Termos, para que a TodosUnidos averigue a situação indicada, e adote as medidas necessárias para a apuração e resolução de eventual irregularidade, se entender pertinente.\n\n    Caso Você seja proprietário de propriedade intelectual a ser disponibilizada por meio da Plataforma, ao aceitar estes Termos, Você concede à TodosUnidos a licença de uso, temporária, não exclusiva, de sua marca, ou quaisquer sinais identificativos necessários para a sua identificação, por meio da Plataforma. Você compreende que suas marcas apenas serão utilizadas para sua identificação na Plataforma, em conformidade com a legislação aplicável.\n\n    Você se declara ciente de que é vedado, tanto a Você quanto à TodosUnidos, ceder, licenciar, vender, negociar ou de qualquer outra forma transferir a utilização das marcas eventualmente disponibilizadas na Plataforma para quaisquer terceiros, bem como auxiliar terceiros a contestar, ou, por qualquer forma, prejudicar, direta ou indiretamente, a validade do nome e das marcas disponibilizadas por meio da Plataforma.\n\n5. Por quais situações a TodosUnidos não poderá ser responsabilizada?\n    A TodosUnidos, em nenhuma hipótese, será responsável:\n        a) Por quaisquer indisponibilidades, erros e/ou falhas eventualmente apresentadas pela Plataforma, inclusive por qualquer defraudação da utilidade que Você possa atribuir à Plataforma, pela sua falibilidade, ou por qualquer dificuldade de acesso enfrentada por Você;\n        b) Por eventuais erros e/ou inconsistências na transmissão de dados, bem como relacionados à qualidade ou disponibilidade da conexão de internet, capazes de obstar o adequado recebimento de informações por Você ou pela TodosUnidos;\n        c) Por dados desatualizados, incompletos e/ou inverídicos eventualmente fornecidos por meio da Plataforma;\n        d) Pelo uso da Plataforma em desacordo com o disposto neste Termos;\n        e) Pelas negociações realizadas em ambiente externo à Plataforma;\n        f) Pela oferta de produtos e/ou serviços impróprios, imorais ou ilegais, por meio da Plataforma, os quais serão de sua responsabilidade exclusiva;\n        g) Por quaisquer danos e prejuízos de toda natureza, decorrentes do conhecimento que terceiros não autorizados possam ter de quaisquer dados fornecidos por meio do Aplicativo, em decorrência de falha exclusivamente relacionada a Você ou a terceiros que fujam a qualquer controle razoável da TodosUnidos.\n\n6. Como serão tratados, armazenados e destinados seus dados pessoais coletados pela TodosUnidos?\n    Durante o uso da Plataforma, a TodosUnidos coletará e armazenará informações ativamente inseridas por Você, conforme indicado no item 13 destes Termos, bem como demais informações geradas automaticamente, tais quais características do dispositivo de acesso, do navegador, Protocolo de Internet (IP, com data e hora), dados de geolocalização, dentre outros.\n\n    Todos os dados coletados por meio da Plataforma são considerados confidenciais pela TodosUnidos, comprometendo-se esta a adotar adequadas medidas de segurança e conferir tratamento e armazenamento apropriados, conforme disposto nestes Termos.\n\n    As informações coletadas pela TodosUnidos são e serão destinadas (i) a desenvolver, manter e aperfeiçoar os recursos e funcionalidades da Plataforma a fim de aprimorar a experiência do usuário, (ii) para a elaboração de relatórios estatísticos e (iii) permitir a comunicação entre Você e a TodosUnidos, inclusive mediante o envio e recebimento de e-mails, contatos telefônicos e/ou envio de mensagens pelas redes sociais.\n\n    Nesse sentido, Você manifesta, por meio do aceite a este documento, seu consentimento livre, expresso, informado e inequívoco para que os dados coletados sejam tratados da forma descrita nestes Termos, acima.\n\n7. Em que circunstâncias meus dados pessoais poderão ser compartilhados?\n    A TodosUnidos poderá compartilhar os dados coletados por meio da Plataforma, nas seguintes hipóteses:\n        a) Quando necessário à disponibilização da Plataforma;\n        b) De modo a proteger os interesses da TodosUnidos, em qualquer espécie de conflito, inclusive demandas judiciais; ou\n        c) Mediante ordem judicial ou por requerimento de autoridades administrativas que detenham competência legal para sua requisição.\n    \n    Ainda, Você reconhece que os dados coletados pela TodosUnidos por meio da Plataforma serão armazenados em servidores próprios ou de empresas contratadas para este propósito, sendo empregados todos os esforços razoáveis de mercado para garantir a segurança dos referidos sistemas na guarda de suas informações, entre eles aqueles que atendem às diretrizes sobre padrões de segurança estabelecidas na legislação brasileira, quais sejam:\n        a) Utilização de métodos padrões de mercado para criptografar os dados coletados para garantir sua inviolabilidade;\n        b) Emprego de softwares de alta tecnologia para proteção contra o acesso não autorizado aos sistemas, sendo estes considerados ambientes controlados e de segurança;\n        c) Disponibilização de acesso a locais de armazenamento de dados pessoais apenas a pessoas previamente autorizadas, comprometidas ao sigilo de tais dados, inclusive mediante a assinatura de termo de confidencialidade;\n        d) Aplicação de mecanismos de autenticação de acesso aos registros capazes de individualizar o responsável pelo tratamento e acesso dos dados coletados em decorrência da utilização da Plataforma; e,\n        e) Manutenção de inventário indicando momento, duração, identidade do funcionário ou do responsável pelo acesso e o arquivo objeto, com base nos registros de conexão e de acesso a aplicações, conforme determinado no artigo 13 do Decreto nº 8.771/2016.\n\n8. Quais os meus direitos em relação aos meus dados pessoais?\n    Você poderá requerer à TodosUnidos os seguintes direitos:\n        a) A confirmação da existência de tratamento de seus dados pessoais e o acesso aos seus dados coletados por meio da Plataforma;\n        b) A correção de seus dados, caso estes estejam incompletos, inexatos ou desatualizados;\n        c) O bloqueio ou eliminação de dados desnecessários, excessivos ou tratados em desconformidade com a legislação brasileira aplicável;\n        d) A portabilidade de seus dados pessoais, para si ou para terceiro;\n        e) A eliminação dos dados pessoais tratados com o seu consentimento, desde que não haja determinação legal para mantê-los registrados junto à TodosUnidos;\n        f) A obtenção de informações sobre entidades públicas ou privadas com as quais a TodosUnidos compartilhou seus dados; e,\n        g) Informações sobre a possibilidade de não fornecer o seu consentimento, bem como de ser informado sobre suas consequências, em caso de negativa.\n\n    Nesse sentido, para que Você possa exercer seus direitos, basta que entre em contato conosco por meio de um de nossos Canais de Atendimento indicados no item 13 destes Termos, apontando suas dúvidas e/ou requerimentos relacionados a seus dados pessoais, sendo certo que a TodosUnidos empregará os melhores esforços para atendê-lo no menor tempo possível.\n\n9. Por quanto tempo a Plataforma será disponibilizada?\n    A Plataforma será disponibilizada pela TodosUnidos por prazo indeterminado. Contudo, isto não significa dizer que a mesma estará disponível eternamente, sendo certo que a TodosUnidos poderá, a qualquer tempo, a seu exclusivo critério, descontinuar a Plataforma, sem necessidade de avisá-lo previamente e sem que lhe seja devida qualquer indenização em razão disso.\n\n    Ademais, a TodosUnidos também poderá, a qualquer momento, limitar o seu acesso à Plataforma, sendo-lhe possível negá-lo ou suspendê-lo em caso de suspeita de uso indevido ou ilícito da Plataforma, o que também poderá motivar a exclusão de todas as informações fornecidas, sem que qualquer indenização ou compensação lhe seja devida por conta disso.\n\n10. Atenção aos Links de Terceiros!\n    A Plataforma poderá conter links para website(s) de terceiros. Você está ciente e concorda que a existência destes links não representa um endosso ou patrocínio a sites terceiros e reconhece estar sujeito aos termos de uso e políticas de privacidade de empresas terceiras, os quais deverão ser verificados por Você.\n\n11. Canal de Denúncias\n    Caso Você identifique a disponibilização de qualquer conteúdo em desacordo com estes Termos ou com a legislação vigente, por meio da Plataforma, poderá fazer uma denúncia por meio do e-mail ouvidoria@todosunidos.com.vc.\n\n12. Regras Gerais\n    Uma vez que a Plataforma poderá ser aperfeiçoada, estes Termos estão constantemente sujeitos a aprimoramento, podendo ser alterados a qualquer momento, salvo em caso de vedação legal nesse sentido. Assim, é de sua exclusiva responsabilidade a leitura periódica da versão mais atualizada dos presentes Termos, com base na data indicada ao término do documento. Caso Você eventualmente discorde de quaisquer alterações implementadas, deverá se abster de utilizar a Plataforma.\n\n    Qualquer cláusula ou condição deste instrumento que, por qualquer razão, venha a ser reputada nula ou ineficaz por qualquer juízo ou tribunal, não afetará a validade das demais disposições destes Termos, as quais permanecerão plenamente válidas e vinculantes, gerando efeitos em sua máxima extensão.\n\n    A falha da TodosUnidos em exigir quaisquer direitos ou disposições dos presentes Termos não constituirá renúncia, podendo esta exercer regularmente o seu direito, dentro dos prazos legais.\n\n13. Canais de Atendimento\n    Todas as comunicações encaminhadas por Você à TodosUnidos serão consideradas plenamente válidas quando realizadas por meio de endereços de e-mail com o domínio @todosunidos.com.vc, disponibilizados na Plataforma, especialmente para o endereço ouvidoria@todosunidos.com.vc. Por sua vez, todas as comunicações da TodosUnidos a Você serão consideradas válidas quando realizadas por meio de qualquer das informações de contato disponibilizadas por Você na Plataforma, incluindo-se, mas não limitado a, e-mail, telefone e carta.\n\n14. Legislação e Foro\n    A presente relação jurídica é regida exclusivamente pelas leis brasileiras, inclusive eventuais ações decorrentes de violação dos seus termos e condições.\n\n    Fica eleito o Foro do domicilio do Usuário para dirimir quaisquer dúvidas, questões ou litígios decorrentes dos presentes Termos, renunciando as partes a qualquer outro, por mais privilegiado que seja.';
  static const String terms_title = 'Termos de uso';
}
