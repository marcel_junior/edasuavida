import 'package:edasuavida/constant/style.dart';
import 'package:flutter/material.dart';

class ProjectThemes {
  ProjectThemes._();

  static final TextTheme text = TextTheme(
      title: BlackStyle.large,
      subtitle: BlackStyle.medium,
      body1: RomanStyle.medium);

  static final TextTheme lightText = TextTheme(
      title: RomanStyle.large,
      subtitle: BlackStyle.medium,
      body1: RomanStyle.small);

  static const BoxDecoration default_field_box_decoration = BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.all(Radius.circular(10)),
      boxShadow: [SmoothBoxShadowStyle()]);
}
