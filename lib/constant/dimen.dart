import 'package:flutter/material.dart';

class ProjectDimens {
  ProjectDimens._();

  static const Duration animation_duration_small = Duration(milliseconds: 300);
  static const Duration animation_duration_medium = Duration(milliseconds: 600);

  static const BorderRadius default_radius =
      BorderRadius.all(default_circular_side_radius);

  static const Radius default_circular_side_radius = Radius.circular(28);

  static const double font_size_small = 12;
  static const double font_size_medium = 16;
  static const double font_size_large = 20;

  static const double font_weight_heavy = 700;

  static const double margin_small = 8.0;
  static const double margin_medium = 16.0;
  static const double margin_large = 24.0;

  static const double margin_alt_small = 4.0;
  static const double margin_alt_medium = 12.0;
  static const double margin_alt_large = 20.0;

  static const EdgeInsets default_container_insets =
      EdgeInsets.only(left: 20, right: 20, bottom: 36);

  static const EdgeInsets default_field_insets = EdgeInsets.only(top: 30);

}
