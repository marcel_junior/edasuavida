import 'dart:ui';

import 'package:edasuavida/constant/color.dart';
import 'package:edasuavida/constant/dimen.dart';
import 'package:flutter/material.dart';

class ProjectFonts {
  ProjectFonts._();

  static const String font_default = 'Avenir';
}

class BlackStyle extends TextStyle {
  static const TextStyle normal = const BlackStyle();
  static const TextStyle small = const BlackStyle(fontSize: ProjectDimens.font_size_small);
  static const TextStyle medium =
      const BlackStyle(fontSize: ProjectDimens.font_size_medium);
  static const TextStyle large =
      const BlackStyle(fontSize: ProjectDimens.font_size_large);

  const BlackStyle(
      {String fontFamily = ProjectFonts.font_default,
      double fontSize = ProjectDimens.font_size_medium,
      Color color = ProjectColors.secondary_1,
      TextDecoration textDecoration = TextDecoration.none,
      FontWeight weight = FontWeight.w700})
      : super(
            fontFamily: fontFamily,
            fontSize: fontSize,
            color: color,
            decoration: textDecoration,
            fontWeight: weight);
}

class SmoothBoxShadowStyle extends BoxShadow {
  const SmoothBoxShadowStyle(
      {Color color = ProjectColors.secondary_6,
      double radius = 32,
      spreadRadius = 4.0})
      : super(color: color, blurRadius: radius, spreadRadius: spreadRadius);
}

class RomanStyle extends TextStyle {
  static const TextStyle normal = const RomanStyle();
  static const TextStyle small =
      const RomanStyle(fontSize: ProjectDimens.font_size_small);
  static const TextStyle medium =
      const RomanStyle(fontSize: ProjectDimens.font_size_medium);
  static const TextStyle large =
      const RomanStyle(fontSize: ProjectDimens.font_size_large);

  const RomanStyle(
      {String fontFamily = ProjectFonts.font_default,
      double fontSize = ProjectDimens.font_size_medium,
      Color color = ProjectColors.secondary_3,
      TextDecoration textDecoration = TextDecoration.none,
      FontWeight weight = FontWeight.w300})
      : super(
            fontFamily: fontFamily,
            fontSize: fontSize,
            color: color,
            decoration: textDecoration,
            fontWeight: weight);
}

class HeavyStyle extends TextStyle {
  static const TextStyle normal = const HeavyStyle();
  static const TextStyle small =
      const HeavyStyle(fontSize: ProjectDimens.font_size_small);
  static const TextStyle medium =
      const HeavyStyle(fontSize: ProjectDimens.font_size_medium);
  static const TextStyle large =
      const HeavyStyle(fontSize: ProjectDimens.font_size_large);

  const HeavyStyle(
      {String fontFamily = ProjectFonts.font_default,
      double fontSize = ProjectDimens.font_size_medium,
      Color color = Colors.white,
      TextDecoration textDecoration = TextDecoration.none})
      : super(
          fontFamily: fontFamily,
          fontSize: fontSize,
          color: color,
          decoration: textDecoration,
        );
}
