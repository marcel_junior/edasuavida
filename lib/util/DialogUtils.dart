import 'package:edasuavida/constant/style.dart';
import 'package:edasuavida/widget/RoundedButton.dart';
import 'package:flutter/material.dart';

class DialogUtils {
  static void showSingleMessageDialog(BuildContext context, String title,
      String description, String buttonLabel, Function onItemClicked) {
    showDialog(
        context: context,
        builder: (_) {
          return Container(
              child: Dialog(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
            child: Container(
                padding: EdgeInsets.all(24),
                child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(title, style: BlackStyle(fontSize: 18), textAlign: TextAlign.center),
                Container(
                    padding: EdgeInsets.only(top: 16, bottom: 16),
                    child: Text(description, style: RomanStyle.medium, textAlign: TextAlign.center)),
                RoundedButton(buttonLabel, onItemClicked)
              ],
            )),
          ));
        });
  }
}
