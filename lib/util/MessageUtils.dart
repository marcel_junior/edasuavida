import 'package:url_launcher/url_launcher.dart';

class MessageUtils {

  static Future<void> sendEmail(String email, String subject) async {
    final url = getEmailUrl(email, subject);
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  static getEmailUrl(String email, String subject) {
    final Uri params = Uri(
      scheme: 'mailto',
      path: email,
      query: 'subject=$subject', //add subject and body here
    );
    return params.toString();
  }
}
