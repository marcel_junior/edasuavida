import 'dart:math';

import 'package:firebase_storage/firebase_storage.dart';

class StringUtils {
  static final chars = "abcdefghijklmnopqrstuvwxyz0123456789";

  static isValidEmail(String email) {
    if (RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(email)) {
      return true;
    }
    return false;
  }

  static String randomString(int length) {
    Random rnd = new Random(
        new DateTime.now().millisecondsSinceEpoch + Random().nextInt(900));
    String result = "";
    for (var i = 0; i < length; i++) {
      result += chars[rnd.nextInt(chars.length)];
    }
    return result;
  }

  static getImageUrlFromFirebasePath(String firebaseImagePath) {
    String filename = firebaseImagePath.substring(
        firebaseImagePath.lastIndexOf('/') + 1, firebaseImagePath.length);
    final ref = FirebaseStorage.instance.ref().child(filename);
    return ref.getDownloadURL();
  }
}
