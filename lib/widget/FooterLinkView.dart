import 'dart:io';

import 'package:edasuavida/constant/Constants.dart';
import 'package:edasuavida/constant/color.dart';
import 'package:edasuavida/constant/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FooterLinkView extends StatelessWidget {

  final String firstLine;
  final String secondLine;
  final Function onClick;


  FooterLinkView(this.firstLine, this.secondLine, this.onClick);

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: onClick,
        child: Container(
        padding: EdgeInsets.only(left: 20, right: 20, bottom: Platform.isIOS ? 60 : 24),
        child: Column(
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(bottom: 12),
                child: Divider(color: ProjectColors.secondary_3)),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                RichText(
                  overflow: TextOverflow.ellipsis,
                  text: TextSpan(style: RomanStyle.medium, children: <TextSpan>[
                    TextSpan(text: '$firstLine\n'),
                    TextSpan(
                        text: secondLine,
                        style: RomanStyle(
                            textDecoration: TextDecoration.underline,
                            color: ProjectColors.secondary_1))
                  ]),
                ),
                Image.asset(Constants.icon_arrow_right)
              ],
            )
          ],
        )));
  }
}
