import 'package:edasuavida/constant/Constants.dart';
import 'package:edasuavida/constant/color.dart';
import 'package:edasuavida/constant/dimen.dart';
import 'package:edasuavida/constant/style.dart';
import 'package:edasuavida/data/model/Proposal.dart';
import 'package:edasuavida/util/StringUtils.dart';
import 'package:edasuavida/widget/ImageView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'AnimatedBox.dart';

// ignore: must_be_immutable
class CampaignItemView extends StatefulWidget {
  Proposal _proposal;
  Function(Proposal) _onItemTap;

  CampaignItemView(
      {@required Proposal proposal, onItemTap(Proposal proposal)}) {
    _proposal = proposal;
    _onItemTap = onItemTap;
  }

  @override
  State<StatefulWidget> createState() {
    return CampaignItemViewState();
  }
}

class CampaignItemViewState extends State<CampaignItemView> {
  @override
  void initState() {
    var image =
        widget._proposal.images.isEmpty ? "" : widget._proposal.images[0];
    if (image.isEmpty) {
      widget._proposal.imageURL = image;
      setState(() {
        widget._proposal.imageURL = image;
      });
    } else {
      StringUtils.getImageUrlFromFirebasePath(
              widget._proposal.images.isEmpty ? "" : widget._proposal.images[0])
          .then((value) {
        widget._proposal.imageURL = value;
        setState(() {
          widget._proposal.imageURL = value;
        });
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(
            horizontal: ProjectDimens.margin_large,
            vertical: ProjectDimens.margin_small),
        child: AnimatedBox(
            child: Column(
              children: <Widget>[
                _buildImage(),
                Container(
                  padding:
                      EdgeInsets.only(top: 12, left: 16, right: 19, bottom: 12),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            child: Hero(
                              tag: "title_${widget._proposal.proposalId}",
                              transitionOnUserGestures: true,
                              flightShuttleBuilder: (_,
                                  Animation<double> animation,
                                  HeroFlightDirection flightDirection,
                                  BuildContext fromHeroContext,
                                  BuildContext toHeroContext) {
                                final fromText =
                                    (fromHeroContext.widget as Hero).child
                                        as Text;
                                final toText = (toHeroContext.widget as Hero)
                                    .child as Text;
                                double fromTextSize = fromText.style.fontSize;
                                double toTextSize = toText.style.fontSize;

                                if (flightDirection ==
                                    HeroFlightDirection.push) {
                                  return TextResizeTransition(
                                    sizeAnim: animation,
                                    minFontSize: toTextSize,
                                    maxFontSize: fromTextSize,
                                    child: fromText,
                                  );
                                }

                                return TextResizeTransition(
                                  child: toText,
                                  sizeAnim: animation,
                                  minFontSize: fromTextSize,
                                  maxFontSize: toTextSize,
                                );
                              },
                              child: _buildTitle(),
                            ),
                          ),
//                          Row(
//                            children: <Widget>[
//                              Image.asset(Constants.icon_flag_2),
//                              SizedBox(width: 10),
//                              Image.asset(Constants.icon_flag_2)
//                            ],
//                          )
                        ],
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 8, bottom: 16),
                        child: Text(
                          widget._proposal.description,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
//                      Text('',
//                          style: BlackStyle(
//                              fontSize: 12,
//                              weight: FontWeight.w700,
//                              color: ProjectColors.highlight_3))
                    ],
                  ),
                ),
              ],
            ),
            onActionTap: _onTap));
  }

  Widget _buildImage() => Hero(
        tag: "img_${widget._proposal.proposalId}",
        transitionOnUserGestures: true,
        child: ImageView(
          imgUrl: widget._proposal.imageURL,
          heigth: 150,
          radius: BorderRadius.only(
              topLeft: ProjectDimens.default_circular_side_radius,
              topRight: ProjectDimens.default_circular_side_radius),
        ),
      );

  void _onTap() {
    if (widget._onItemTap != null) {
      widget._onItemTap(widget._proposal);
    }
  }

  Widget _buildTitle() => Text(
        widget._proposal.title,
        style: BlackStyle.medium,
        maxLines: 1,
      );
}

class CampaignItemLoadingView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      margin: new EdgeInsets.symmetric(
          horizontal: ProjectDimens.margin_large,
          vertical: ProjectDimens.margin_small),
      decoration: BoxDecoration(
          color: ProjectColors.gray_1,
          borderRadius: ProjectDimens.default_radius),
    );
  }
}

class TextResizeTransition extends AnimatedWidget {
  final Animation<double> sizeAnim;
  final double minFontSize;
  final double maxFontSize;
  final Text child;

  TextResizeTransition({
    this.sizeAnim,
    this.child,
    this.minFontSize,
    this.maxFontSize,
  }) : super(listenable: sizeAnim);

  @override
  Widget build(BuildContext context) {
    final delta = maxFontSize - minFontSize;
    return Material(
        color: Colors.transparent,
        child: Text(
          child.data,
          style: child.style
              .copyWith(fontSize: maxFontSize - delta * (sizeAnim.value)),
        ));
  }
}
