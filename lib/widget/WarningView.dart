import 'package:edasuavida/constant/color.dart';
import 'package:edasuavida/constant/dimen.dart';
import 'package:edasuavida/constant/style.dart';
import 'package:edasuavida/widget/RoundedButton.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class WarningView extends StatelessWidget {
  String _icon;
  String _title;
  String _subtitle;
  String _actionText;
  Function _onActionTap;

  WarningView(
      {String icon,
      String title,
      String subtitle,
      String actionText,
      onActionTap()}) {
    _icon = icon;
    _title = title;
    _subtitle = subtitle;
    _actionText = actionText;
    _onActionTap = onActionTap;
  }

  @override
  Widget build(BuildContext context) {
    var children = <Widget>[];

    if (_icon != null && _icon.isNotEmpty) {
      children.add(Lottie.asset(
        _icon,
        width: 180,
        height: 180,
        frameBuilder: (context, child, composition) {
          return AnimatedOpacity(
            child: child,
            opacity: composition == null ? 0 : 1,
            duration: const Duration(seconds: 1),
            curve: Curves.easeOut,
          );
        },
      ));
    }

    var textChildren = <Widget>[];
    if (_title != null && _title.isNotEmpty) {
      textChildren.add(Material(color: Colors.transparent, child: Text(_title, style: BlackStyle.large),));
    }
    if (_subtitle != null && _subtitle.isNotEmpty) {
      textChildren.add(Text(_subtitle, style: RomanStyle.medium));
    }
    if (textChildren.isNotEmpty) {
      children.add(Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: textChildren,
      ));
    }

    if (_actionText != null && _actionText.isNotEmpty) {
      children.add(Container(
          margin: new EdgeInsets.symmetric(
              horizontal: ProjectDimens.margin_medium,
              vertical: ProjectDimens.margin_medium),
          child: RoundedButton(_actionText, _onActionTap)));
    }

    return Center(
        child: Container(
      height: 360,
      width: double.infinity,
      margin: new EdgeInsets.symmetric(
          horizontal: ProjectDimens.margin_large,
          vertical: ProjectDimens.margin_large),
      decoration: BoxDecoration(
          color: ProjectColors.gray_1,
          borderRadius: ProjectDimens.default_radius),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: children,
      ),
    ));
  }
}
