import 'package:edasuavida/constant/color.dart';
import 'package:edasuavida/constant/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class LabeledDividerView extends StatelessWidget {

  String _label;

  LabeledDividerView(this._label);

  @override
  Widget build(BuildContext context) {
    return Row(children: <Widget>[
      Expanded(child: Divider(color: ProjectColors.secondary_4)),
      Container(
          padding: EdgeInsets.only(left: 20, right: 20),
          child: Text(_label, style: RomanStyle())),
      Expanded(child: Divider(color: ProjectColors.secondary_4)),
    ]);
  }
}
