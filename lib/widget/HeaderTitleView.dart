import 'package:edasuavida/constant/color.dart';
import 'package:edasuavida/constant/dimen.dart';
import 'package:edasuavida/constant/style.dart';
import 'package:flutter/cupertino.dart';

// ignore: must_be_immutable
class HeaderTitleView extends StatelessWidget {

  String _title;
  String _description;

  HeaderTitleView(this._title, this._description);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
            padding: EdgeInsets.only(bottom: 12, top: 30),
            child: Text(_title,
                style: BlackStyle(fontSize: ProjectDimens.font_size_large))),
        Text(_description, style: RomanStyle()),
      ],
    ));
  }
}
