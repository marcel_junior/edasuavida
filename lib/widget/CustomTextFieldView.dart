import 'package:edasuavida/constant/Constants.dart';
import 'package:edasuavida/constant/color.dart';
import 'package:edasuavida/constant/dimen.dart';
import 'package:edasuavida/constant/style.dart';
import 'package:edasuavida/constant/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../constant/style.dart';

// ignore: must_be_immutable
class CustomTextFieldView extends StatefulWidget {
  String _hintText;
  String _labelText;
  final TextEditingController _textEditingController;
  TextInputType _inputType;
  TextInputAction _inputAction;
  FormFieldValidator _validator;
  bool _passwordType;
  int _maxLength;

  //PasswordField Config
  bool _obscureText = true;

  CustomTextFieldView(this._textEditingController, this._validator,
      {hintText = '',
      labelText = '',
      icon = '',
      passwordType = false,
      inputType = TextInputType.text,
      counterField = false,
      maxLength = 0,
      inputAction = TextInputAction.next}) {
    this._hintText = hintText;
    this._labelText = labelText;
    this._inputType = inputType;
    this._inputAction = inputAction;
    this._passwordType = passwordType;
    this._maxLength = maxLength;
  }

  @override
  State<StatefulWidget> createState() {
    return CustomTextFieldViewState();
  }
}

class CustomTextFieldViewState extends State<CustomTextFieldView> {
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: ProjectDimens.default_field_insets,
        padding: widget._maxLength != 0 ? EdgeInsets.only(bottom: 5) : null,
        decoration: ProjectThemes.default_field_box_decoration,
        child: TextFormField(
          maxLength: widget._maxLength == 0 ? null : widget._maxLength,
          enableInteractiveSelection: widget._passwordType ? false : true,
          obscureText: widget._passwordType ? widget._obscureText : false,
          onFieldSubmitted: widget._inputAction == TextInputAction.next
              ? (_) => FocusScope.of(context).nextFocus()
              : (_) {},
          textInputAction: widget._inputAction,
          keyboardType: widget._inputType,
          validator: widget._validator,
          controller: widget._textEditingController,
          style: RomanStyle(
              color: ProjectColors.secondary_1,
              fontSize: ProjectDimens.font_size_large),
          cursorWidth: 1,
          cursorColor: ProjectColors.primary_1,
          decoration: InputDecoration(
              suffixIcon: widget._passwordType
                  ? IconButton(
                      icon: Image.asset(Constants.icon_password_hide),
                      onPressed: () {
                        setState(() {
                          widget._obscureText = !widget._obscureText;
                        });
                      },
                    )
                  : null,
              hasFloatingPlaceholder: false,
              errorStyle: RomanStyle(
                  color: Colors.red, fontSize: ProjectDimens.font_size_small),
              hintText: widget._labelText.isEmpty
                  ? widget._hintText
                  : widget._labelText,
              labelText: widget._hintText,
              counterStyle: TextStyle(height: double.minPositive),
              hintStyle: RomanStyle.large,
              focusColor: Colors.white,
              fillColor: Colors.white,
              labelStyle: RomanStyle.large,
              errorBorder: getFieldBorder(getErrorBorderSide()),
              focusedErrorBorder: getFieldBorder(getErrorBorderSide()),
              focusedBorder: getFieldBorder(getTransparentBorderSide()),
              enabledBorder: getFieldBorder(getTransparentBorderSide()),
              border: getFieldBorder(getTransparentBorderSide())),
        ));
  }

  getTransparentBorderSide() {
    return BorderSide(color: Colors.transparent, width: 0.0);
  }

  getErrorBorderSide() {
    return BorderSide(color: Colors.red, width: 2.0);
  }

  getFieldBorder(BorderSide borderSide) {
    return OutlineInputBorder(
        borderSide: borderSide,
        gapPadding: 0,
        borderRadius: BorderRadius.circular(10));
  }
}
