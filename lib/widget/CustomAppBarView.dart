import 'package:edasuavida/constant/Constants.dart';
import 'package:edasuavida/constant/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class CustomAppBarView extends StatelessWidget with PreferredSizeWidget  {

  String _title;
  bool _shareAction;
  bool _favoriteAction;
  Function _onSharePressed;
  Function _onFavoritePressed;
  Function _onPressed;

  CustomAppBarView(this._onPressed, {String title = '',
    bool shareAction = false,
    bool favoriteAction = false,
    onSharePressed,
    onFavoritePressed}) {
    this._title = title;
    this._shareAction = shareAction;
    this._favoriteAction = favoriteAction;
    this._onSharePressed = onSharePressed;
    this._onFavoritePressed = onFavoritePressed;
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
        actions: <Widget>[
          _favoriteAction
              ? IconButton(
            onPressed: _onFavoritePressed,
            icon: Image.asset(Constants.icon_heart),
          )
              : SizedBox(),
          _shareAction
              ? IconButton(
            onPressed: _onSharePressed,
            icon: Image.asset(Constants.icon_share),
          )
              : SizedBox()
        ],
        title: Text(_title, style: BlackStyle.large),
        leading: IconButton(
            icon: Image.asset(Constants.image_back_button),
            onPressed: _onPressed),
        backgroundColor: Colors.transparent,
        elevation: 0);
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);

}