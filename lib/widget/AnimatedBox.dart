import 'dart:ui';

import 'package:edasuavida/constant/color.dart';
import 'package:edasuavida/constant/dimen.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class AnimatedBox extends StatefulWidget {
  Widget _child;
  Color _shadow;
  Color _shadowPressed;
  Color _background;
  Color _highlightColor;
  Color _rippleColor;
  Duration _duration;
  BorderRadiusGeometry _borderRadius;
  double _elevation;
  double _elevationPressed;
  bool _pressed;
  Function _onActionTap;

  AnimatedBox(
      {Widget child,
      Color shadow = ProjectColors.shadow_1,
      Color shadowPressed = ProjectColors.shadow_2,
      Color background = Colors.white,
      Color highlightColor = ProjectColors.highlight_1,
      Color rippleColor = ProjectColors.secondary_6,
      Duration duration = const Duration(milliseconds: 150),
      BorderRadiusGeometry borderRadius = ProjectDimens.default_radius,
      double elevation = 8.0,
      double elevationPressed = 12.0,
      @required Function onActionTap,
      bool pressed = false}) {
    this._child = child;
    this._shadow = shadow;
    this._shadowPressed = shadowPressed;
    this._background = background;
    this._highlightColor = highlightColor;
    this._rippleColor = rippleColor;
    this._duration = duration;
    this._borderRadius = borderRadius;
    this._elevation = elevation;
    this._elevationPressed = elevationPressed;
    this._onActionTap = onActionTap;
    this._pressed = pressed;
  }

  @override
  _AnimatedBox createState() {
    return _AnimatedBox();
  }
}

class _AnimatedBox extends State<AnimatedBox> {
  @override
  Widget build(BuildContext context) {
    return AnimatedPhysicalModel(
      duration: widget._duration,
      curve: Curves.easeIn,
      shape: BoxShape.rectangle,
      color: Colors.transparent,
      animateShadowColor: true,
      shadowColor: widget._pressed ? widget._shadowPressed : widget._shadow,
      borderRadius: widget._borderRadius,
      elevation: widget._pressed ? widget._elevationPressed : widget._elevation,
      child: Material(
        color: widget._background,
        borderRadius: widget._borderRadius,
        child: InkWell(
          splashColor: widget._rippleColor,
          highlightColor: widget._highlightColor,
          onTap: widget._onActionTap,
          onHighlightChanged: (valueChanged) {
            setState(() {
              widget._pressed = valueChanged;
            });
          },
          borderRadius: widget._borderRadius,
          child: widget._child,
        ),
      ),
    );
  }
}
