import 'package:edasuavida/constant/color.dart';
import 'package:edasuavida/constant/dimen.dart';
import 'package:edasuavida/constant/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../constant/style.dart';

// ignore: must_be_immutable
class SmoothImageButton extends StatelessWidget {
  String _icon;
  String _title;
  Function _onPressed;

  SmoothImageButton(this._icon, this._title, this._onPressed);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 48,
        decoration: BoxDecoration(
            color: Colors.transparent,
            borderRadius: BorderRadius.all(Radius.circular(28)),
            boxShadow: [SmoothBoxShadowStyle()]),
        child: RaisedButton(
          onPressed: _onPressed,
          elevation: 1,
          highlightElevation: 8,
          splashColor: ProjectColors.secondary_6,
          highlightColor: ProjectColors.highlight_1,
          color: Colors.white,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
          child: Stack(
            children: <Widget>[
              Align(
                alignment: Alignment.centerLeft,
                child: Image.asset(_icon),
              ),
              Align(
                alignment: Alignment.center,
                child: Text(_title,
                    style:
                        RomanStyle(fontSize: ProjectDimens.font_size_medium)),
              )
            ],
          ),
        ));
  }
}
