import 'package:edasuavida/constant/Constants.dart';
import 'package:edasuavida/constant/dimen.dart';
import 'package:edasuavida/constant/style.dart';
import 'package:edasuavida/constant/theme.dart';
import 'package:edasuavida/data/model/ChipsFilterItem.dart';
import 'package:edasuavida/feature/BaseLoadedView.dart';
import 'package:edasuavida/feature/campaign/CampaignController.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'ChipsFilter.dart';
import 'FilterButton.dart';
import 'RoundedButton.dart';

// ignore: must_be_immutable
class CampaignFiltersView extends StatefulWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey;

  CampaignController _controller = CampaignController();

  bool isTypeFilterSelected = false;
  final Function onTypeFilterApplied;

  CampaignFiltersView(this.onTypeFilterApplied, this._scaffoldKey);

  @override
  State<StatefulWidget> createState() {
    return CampaignFilterViewsState();
  }
}

class CampaignFilterViewsState extends State<CampaignFiltersView>
    with BaseLoadedViewHelper {
  List<ChipsFilterItem> _typeFilterItems = List();

  @override
  void initState() {
    super.initState();
    withToolbar = false;
    _getTags();
  }

  @override
  Widget build(BuildContext context) {
    /*TODO Transform in Horizontal Scroll to another filters*/
    return FilterButton(Constants.icon_type_filter,
        Constants.icon_type_filter_selected, widget.isTypeFilterSelected,
        title: Constants.type, onItemClicked: () {
      _onTypeFilterClicked();
    });
  }

  _onTypeFilterClicked() {
    _buildTypeFilterBottomSheet(context);
  }

  _buildTypeFilterBottomSheet(final BuildContext context) {
    widget._scaffoldKey.currentState.showBottomSheet((context) {
      return getCurrentWidget();
    }, elevation: 16.0,
      shape: RoundedRectangleBorder(side: BorderSide(color: Colors.white70), borderRadius: BorderRadius.circular(30))
    );

//    showModalBottomSheet(
//        context: context,
//        builder: (builder) {
//          return getCurrentWidget();
//        });
  }

  _buildTypeFilterWidget() => Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: ProjectDimens.default_circular_side_radius,
              topRight: ProjectDimens.default_circular_side_radius)),
      padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(Constants.help_type, style: BlackStyle.large),
              InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Padding(
                      padding: EdgeInsets.all(10),
                      child: Image.asset(Constants.icon_close)))
            ],
          ),
          Padding(
              padding: EdgeInsets.only(top: 10, bottom: 20),
              child: Container(
                  height: MediaQuery.of(context).size.height / 3,
                  child: Stack(
                    children: <Widget>[
                      Container(child: ChipsFilter(_typeFilterItems)),
                      Align(
                          alignment: Alignment.bottomCenter,
                          child: Container(
                            height: 10,
                            width: double.infinity,
                            decoration: BoxDecoration(boxShadow: [
                              SmoothBoxShadowStyle(spreadRadius: 13.0)
                            ]),
                          ))
                    ],
                  ))),
          RoundedButton(Constants.apply_filter, () {
            final List<String> tagList = toTagList(_typeFilterItems);
            setState(() {
              widget.isTypeFilterSelected = tagList.isNotEmpty;
            });
            widget.onTypeFilterApplied(tagList);
            Navigator.pop(context);
          })
        ],
      ));

  _getTags() {
    setState(() {
      widget._controller.getTags(() {
        status = ScreenStatus.LOADING;
      }, (result) {
        toChipsFilterItemList(result);
        status = ScreenStatus.LOADED;
      }, (error) {
        status = ScreenStatus.ERROR;
      });
    });
  }

  toChipsFilterItemList(List<String> tags) => tags.forEach((tag) {
        _typeFilterItems.add(ChipsFilterItem(tag));
      });

  toTagList(List<ChipsFilterItem> items) {
    List<String> tagList = List();
    items.forEach((item) {
      if (item.isSelected) tagList.add(item.text);
    });
    return tagList;
  }

  @override
  Widget onLoaded() => _buildTypeFilterWidget();

  @override
  onEmptyViewRetryClicked() {
    _getTags();
  }

  @override
  onBackPressed() {}

  @override
  onServerErrorRetryClicked() {
    _getTags();
  }
}
