import 'package:edasuavida/constant/Constants.dart';
import 'package:edasuavida/constant/color.dart';
import 'package:edasuavida/constant/dimen.dart';
import 'package:edasuavida/constant/style.dart';
import 'package:edasuavida/constant/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class CustomDropdownView extends StatefulWidget {

  String _hint;
  List<String> _values;

  CustomDropdownView(this._hint, this._values);

  @override
  State<StatefulWidget> createState() {
    return CustomDropdownViewState();
  }
}

class CustomDropdownViewState extends State<CustomDropdownView> {
  String _selectedItem;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: ProjectDimens.default_field_insets,
        height: 68,
        alignment: Alignment.center,
        padding: EdgeInsets.only(left: 13, right: 15),
        decoration: ProjectThemes.default_field_box_decoration,
        child: DropdownButton<String>(
          isDense: true,
            value: _selectedItem,
            icon: Image.asset(Constants.icon_arrow_down),
            hint: Container(child: Text(widget._hint,
                style: RomanStyle(
                    fontSize: ProjectDimens.font_size_large))),
            isExpanded: true,
            underline: Container(),
            items: widget._values.map((String dropDownStringItem) {
              return DropdownMenuItem<String>(
                value: dropDownStringItem,
                child: Container(child: Text(dropDownStringItem, style: RomanStyle(fontSize: ProjectDimens.font_size_large, color: ProjectColors.secondary_1))),
              );
            }).toList(),
            onChanged: (value) {
              setState(() {
                _selectedItem = value;
              });
            }));
  }
}
