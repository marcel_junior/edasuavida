import 'package:edasuavida/constant/color.dart';
import 'package:edasuavida/constant/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RoundedButton extends StatefulWidget {

  String _text;
  Function _onPressed;

  RoundedButton(this._text, this._onPressed);

  @override
  State<StatefulWidget> createState() {
    return RoundedButtonState();
  }
}

class RoundedButtonState extends State<RoundedButton> {
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
          onPressed: widget._onPressed,
          elevation: 0,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
        padding: EdgeInsets.all(0.0),
          child: Ink(
            decoration: BoxDecoration(
                gradient: ProjectColors.gradient_1,
                borderRadius: BorderRadius.circular(30.0)),
            child: Container(
              alignment: Alignment.center,
              width: double.infinity,
              height: 54,
              child: Text(widget._text, style: BlackStyle(color: Colors.white)),
            )));
  }
}
