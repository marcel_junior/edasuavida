import 'package:cached_network_image/cached_network_image.dart';
import 'package:edasuavida/constant/Constants.dart';
import 'package:edasuavida/constant/dimen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class ImageView extends StatelessWidget {
  final String imgUrl;
  final String placeholder;
  final String error;
  final double width;
  final double heigth;
  final BorderRadius radius;

  ImageView(
      {@required this.imgUrl,
      this.placeholder = Constants.lottie_progress_image,
      this.error = Constants.lottie_error_image,
      this.width = double.infinity,
      this.heigth = double.infinity,
      this.radius = ProjectDimens.default_radius});

  @override
  Widget build(BuildContext context) {
    Widget child = Lottie.asset(placeholder);

    if (imgUrl != null && imgUrl.isNotEmpty) {
      child = CachedNetworkImage(
        height: double.infinity,
        width: double.infinity,
        imageUrl: imgUrl,
        fit: BoxFit.cover,
        placeholder: (context, url) => Lottie.asset(placeholder),
        errorWidget: (context, url, error) => _errorState(),
      );
    }

    return Container(
      width: width,
      height: heigth,
      child: ClipRRect(
        borderRadius: radius,
        child: child,
      ),
    );
  }

  Widget _errorState() => Container(
        margin: EdgeInsets.all(35),
        width: width,
        height: heigth,
        alignment: Alignment.center,
        child: Lottie.asset(this.error, repeat: false, fit: BoxFit.cover),
      );
}
