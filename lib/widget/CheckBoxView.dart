import 'package:edasuavida/constant/Constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CheckBoxView extends StatelessWidget {

  RichText _text;
  Function _onChanged;
  bool _isChecked = false;

  CheckBoxView(this._text, this._isChecked, this._onChanged(bool isChecked));

  @override
  Widget build(BuildContext context) {
    return CheckboxListTile(
      title: _text,
      onChanged: _onChanged,
      value: _isChecked,
      controlAffinity: ListTileControlAffinity.leading,
    );
  }
}
