import 'package:edasuavida/constant/Constants.dart';
import 'package:edasuavida/constant/style.dart';
import 'package:edasuavida/util/MessageUtils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactItemView extends StatelessWidget {

  String title;
  String description;

  ContactItemView(this.title, this.description);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Material(
            child: InkWell(
                onTap: () => onItemTap(title, description),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          /*TODO Transform in Enum*/
                          Text(title == "URL" ? "Site" : title, style: BlackStyle.medium),
                          Linkify(
                            text: description,
                            style: RomanStyle.medium,
                            options:
                                LinkifyOptions(humanize: false, removeWww: true),
                          )
                        ]),
//                    Image.asset(Constants.icon_arrow_right)
                  ],
                ))));
  }

  /*TODO Transform in Enum*/
  onItemTap(type, value) {
    String url;
    switch (type) {
      case "Phone":
        url = "tel:$value";
        break;
      case "Email":
        url = MessageUtils.getEmailUrl(value, "Quero ajudar");
        break;
      default:
        url = value;
    }
    launch(url);
  }
}



