import 'package:edasuavida/data/model/ChipsFilterItem.dart';
import 'package:edasuavida/widget/ChipsFilterItemView.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ChipsFilter extends StatefulWidget {
  final List<ChipsFilterItem> options;

  ChipsFilter(this.options);

  @override
  State<StatefulWidget> createState() {
    return _ChipsFilter();
  }
}

class _ChipsFilter extends State<ChipsFilter> {
  @override
  Widget build(BuildContext context) {
    return GridView.count(
      childAspectRatio: 2,
      shrinkWrap: true,
      crossAxisCount: 3,
      children: List.generate(widget.options.length, (index) {
        ChipsFilterItem item = widget.options[index];
        return Container(
            child: ChipsFilterItemView(
                text: item.text,
                isSelected: item.isSelected,
                onClicked: () {
                  setState(() {
                    widget.options[index].isSelected =
                        !widget.options[index].isSelected;
                  });
                }));
      }),
    );
  }
}
