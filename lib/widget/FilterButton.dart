import 'package:edasuavida/constant/color.dart';
import 'package:edasuavida/constant/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FilterButton extends StatefulWidget {
  final String iconPath;
  final String iconSelectedPath;
  final String title;
  final Function onItemClicked;
  final bool isSelected;

  const FilterButton(
      this.iconPath,
      this.iconSelectedPath,
      this.isSelected,
      {Key key,
      this.title,
      this.onItemClicked})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return FilterButtonState();
  }
}

class FilterButtonState extends State<FilterButton> {
  @override
  Widget build(BuildContext context) {
    return RaisedButton.icon(
      onPressed: widget.onItemClicked,
      elevation: 0,
      icon: widget.isSelected
          ? Image.asset(widget.iconSelectedPath)
          : Image.asset(widget.iconPath),
      label: Text(widget.title,
          style: RomanStyle(
              weight: FontWeight.w600,
              color:
                  widget.isSelected ? Colors.white : ProjectColors.highlight_2,
              fontSize: 14)),
      color: widget.isSelected ? ProjectColors.highlight_2 : Colors.white,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
          side: BorderSide(color: widget.isSelected ? ProjectColors.highlight_2 : ProjectColors.secondary_5)),
    );
  }
}
