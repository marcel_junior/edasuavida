import 'package:edasuavida/constant/color.dart';
import 'package:edasuavida/constant/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChipsFilterItemView extends StatelessWidget {

  final String text;
  final bool isSelected;
  final Function onClicked;

  const ChipsFilterItemView(
      {Key key, this.text, this.isSelected = false, this.onClicked})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(5),
        child: RaisedButton(
          elevation: 0,
      color: isSelected ? Colors.white : ProjectColors.secondary_6,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
          side: BorderSide(color: isSelected ? ProjectColors.highlight_2 : ProjectColors.secondary_6,
          width: 3)),
      child: Text(
          text,
          textAlign: TextAlign.center,
          overflow: TextOverflow.ellipsis,
          maxLines: 2,
          style: RomanStyle(
          weight: FontWeight.w600,
          color: isSelected ? ProjectColors.highlight_2 : ProjectColors.secondary_3,
          fontSize: 11
      )),
      onPressed: onClicked,
    ));
  }
}
