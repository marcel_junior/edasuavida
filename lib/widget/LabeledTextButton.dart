


import 'package:edasuavida/constant/style.dart';
import 'package:flutter/cupertino.dart';

// ignore: must_be_immutable
class LabeledTextButton extends StatelessWidget {

  String _text;
  Function _onClicked;

  LabeledTextButton(this._text, this._onClicked);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: _onClicked,
        child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
      Text(_text, style: RomanStyle(textDecoration: TextDecoration.underline))
    ]));
  }

}