import 'package:edasuavida/constant/Constants.dart';
import 'package:edasuavida/constant/color.dart';
import 'package:flutter/material.dart';

import '../constant/style.dart';
import 'AnimatedBox.dart';

// ignore: must_be_immutable
class ChooseAWayItemView extends StatefulWidget {
  String _chooseAwayItemViewImage;
  String _chooseAwayItemViewTitle;
  String _chooseAwayItemViewDescription;
  String _chooseAwayItemViewFlag;
  bool _chooseAwayItemViewEnabled;
  Function _onItemClicked;

  ChooseAWayItemView(
      this._chooseAwayItemViewImage,
      this._chooseAwayItemViewTitle,
      this._chooseAwayItemViewDescription,
      this._chooseAwayItemViewFlag,
      this._chooseAwayItemViewEnabled,
      this._onItemClicked);

  @override
  _ChooseAWayItemView createState() {
    return _ChooseAWayItemView();
  }
}

class _ChooseAWayItemView extends State<ChooseAWayItemView> {
  @override
  Widget build(BuildContext context) {
    var children = <Widget>[];

    if (widget._chooseAwayItemViewEnabled) {
      children.add(_buildContent());
    } else {
      children.add(ColorFiltered(
        colorFilter:
            ColorFilter.mode(ProjectColors.disabled_1, BlendMode.saturation),
        child: _buildContent(),
      ));
    }

    if (widget._chooseAwayItemViewFlag != null &&
        widget._chooseAwayItemViewFlag.isNotEmpty) {
      children.add(_buildFlag());
    }
    Widget content = Stack(
      children: children,
    );

    return Container(
      padding: EdgeInsets.only(left: 20, right: 20),
      child: AnimatedBox(
        background: widget._chooseAwayItemViewEnabled
            ? Colors.white
            : ProjectColors.disabled_1,
        elevation: widget._chooseAwayItemViewEnabled
            ? 8.0
            : 4.0,
        onActionTap:
            widget._chooseAwayItemViewEnabled ? widget._onItemClicked : null,
        child: content,
      ),
    );
  }

  Widget _buildContent() => Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Image(image: AssetImage(widget._chooseAwayItemViewImage)),
          Container(
              padding: EdgeInsets.only(top: 20, bottom: 4),
              child: Text(widget._chooseAwayItemViewTitle,
                  style: BlackStyle.medium.copyWith(
                    color: widget._chooseAwayItemViewEnabled
                        ? ProjectColors.secondary_1
                        : ProjectColors.secondary_4,
                  ))),
          Text(
            widget._chooseAwayItemViewDescription,
            style: RomanStyle.medium.copyWith(
              color: widget._chooseAwayItemViewEnabled
                  ? ProjectColors.secondary_3
                  : ProjectColors.secondary_4,
            ),
          )
        ],
      ),
      padding: EdgeInsets.fromLTRB(16, 20, 16, 20));

  Widget _buildFlag() => Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Container(
              margin: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
              decoration: BoxDecoration(
                border: Border.all(
                    color: ProjectColors.success,
                    width: 1.0,
                    style: BorderStyle.solid),
                borderRadius: BorderRadius.all(Radius.circular(12)),
              ),
              child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                  child: Row(
                    children: <Widget>[
                      Image.asset(
                        Constants.icon_flag,
                        fit: BoxFit.fill,
                        height: 16,
                        width: 16,
                      ),
                      SizedBox(width: 8),
                      Text(
                        widget._chooseAwayItemViewFlag,
                        style: BlackStyle.medium
                            .copyWith(color: ProjectColors.success),
                      ),
                    ],
                  ))),
        ],
      );
}
