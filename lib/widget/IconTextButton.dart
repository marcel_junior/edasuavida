import 'package:edasuavida/constant/color.dart';
import 'package:edasuavida/constant/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class IconTextButton extends StatelessWidget {
  final String iconPath;
  final String title;
  final Function onClick;

  const IconTextButton({Key key, this.iconPath, this.title, this.onClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: onClick,
        child: Row(
      children: <Widget>[
        Image.asset(iconPath),
        Padding(
            padding: EdgeInsets.only(left: 10),
            child: Text(title,
            style: RomanStyle(
                weight: FontWeight.w600,
                textDecoration: TextDecoration.underline,
                color: ProjectColors.secondary_1)))
      ],
    ));
  }
}
