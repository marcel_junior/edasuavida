import 'package:edasuavida/constant/Constants.dart';
import 'package:edasuavida/constant/color.dart';
import 'package:edasuavida/constant/theme.dart';
import 'package:edasuavida/data/storage/projectDatabase.dart';
import 'package:edasuavida/feature/campaign/CampaignDetailView.dart';
import 'package:edasuavida/feature/chooseaway/ChooseAWayView.dart';
import 'package:edasuavida/feature/intro/IntroView.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Router.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SharedPreferences.getInstance();
  FlutterError.onError = Crashlytics.instance.recordFlutterError;
  bool hasOnBoardingBurned = await ProjectDatabase.isOnBoardingBurned();
  runApp(MyApp(hasOnBoardingBurned: hasOnBoardingBurned));
}

class MyApp extends StatefulWidget {
  final bool hasOnBoardingBurned;

  MyApp({this.hasOnBoardingBurned = false});

  @override
  _MyApp createState() => _MyApp();
}

class _MyApp extends State<MyApp> with WidgetsBindingObserver {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: Constants.app_name,
      theme: ThemeData(
          primarySwatch: ProjectColors.primary,
          primaryColor: ProjectColors.primary_1,
          textTheme: ProjectThemes.text),
      home: widget.hasOnBoardingBurned ? ChooseAwayView() : IntroView(),
      onGenerateRoute: Router.generateRoute,
      routes: {
        Constants.view_campaign_detail: (context) => CampaignDetailView(),
      },
    );
  }

  @override
  initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      FlutterStatusbarcolor.setStatusBarWhiteForeground(false);
    }
    super.didChangeAppLifecycleState(state);
  }
}
