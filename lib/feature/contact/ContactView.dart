import 'package:edasuavida/constant/Constants.dart';
import 'package:edasuavida/constant/color.dart';
import 'package:edasuavida/constant/dimen.dart';
import 'package:edasuavida/data/model/Contact.dart';
import 'package:edasuavida/data/model/Proposal.dart';
import 'package:edasuavida/feature/BaseLoadedStatefulView.dart';
import 'package:edasuavida/widget/ContactItemView.dart';
import 'package:edasuavida/widget/CustomAppBarView.dart';
import 'package:edasuavida/widget/HeaderTitleView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:share/share.dart';

import 'ContactController.dart';

class ContactView extends StatefulWidget {
  Proposal proposal;

  ContactView({this.proposal});

  @override
  State<StatefulWidget> createState() {
    return ContactViewState();
  }
}

class ContactViewState extends BaseLoadedStatefulViewState<ContactView> {
  List<Contact> _contactList;

  ContactController controller = ContactController();

  @override
  void initState() {
    super.initState();
    _getContacts();
  }

  @override
  Widget onLoaded() {
    return Scaffold(
      appBar: CustomAppBarView(
          () {
            Navigator.pop(context);
          },
          shareAction: true,
          onSharePressed: () {
            onSharePressedClicked();
          }),
      body: Container(
          padding: ProjectDimens.default_container_insets,
          child: Column(
            children: <Widget>[
              HeaderTitleView(Constants.how_to_help_Title,
                  Constants.how_to_help_description),
              Expanded(
                child: ListView.separated(
                    padding: EdgeInsets.only(top: 40),
                    itemBuilder: (BuildContext context, int index) {
                      return AnimationConfiguration.staggeredList(
                          position: index,
                          duration: const Duration(milliseconds: 375),
                          child: SlideAnimation(
                            verticalOffset: 50.0,
                            child: FadeInAnimation(
                              child: ContactItemView(
                                  _contactList[index].contactType,
                                  _contactList[index].contact),
                            ),
                          ));
                    },
                    separatorBuilder: (BuildContext context, int index) =>
                        Container(
                            padding: EdgeInsets.only(top: 20, bottom: 20),
                            child: Divider(color: ProjectColors.secondary_4)),
                    itemCount: _contactList.length),
              )
            ],
          )),
    );
  }

  @override
  onEmptyViewRetryClicked() {
    _getContacts();
  }

  @override
  onServerErrorRetryClicked() {
    _getContacts();
  }

  _getContacts() {
    controller.getContactList(widget.proposal.proposalId,
        onLoadingWrapperCallback, onErrorWrapperCallback, (result) {
      onLoadedWrapperCallback(() {
        _contactList = result;
      });
    });
  }

  onSharePressedClicked() {
    Share.share(
        controller.getShareFormattedMessageToShare(
            widget.proposal.title, _contactList),
        subject: widget.proposal.title);
  }
}
