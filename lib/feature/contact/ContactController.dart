import 'package:edasuavida/constant/Constants.dart';
import 'package:edasuavida/data/ResquestCallbackWrapper.dart';
import 'package:edasuavida/data/model/Contact.dart';
import 'package:edasuavida/data/network/projectApi.dart';

class ContactController {

  getContactList(String proposalId, onLoading(),onError(Exception error),
      onSuccess(result)) {
    CallbackRequestWrapper<List<Contact>> _observer = wrapRequest();
    _observer.onLoading(onLoading).onError(onError).onSuccess(onSuccess);
    _observer.run((callback) => ProjectApi.instance.getContactsToShareList(proposalId, callback));
  }

  getShareFormattedMessageToShare(String campaignName, List<Contact> list) {
    return Constants.share_message_1 + ' ' + campaignName + ':\n\n' + _formatListToShare(list) + '\n' + Constants.share_message_2;
  }

  _formatListToShare(List<Contact> list) {
    String formattedMessage = '';
    list.forEach((contact) {
      formattedMessage += contact.contactType + ': ' + contact.contact + '\n';
    });
    return formattedMessage;
  }
}
