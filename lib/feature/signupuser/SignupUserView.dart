import 'package:edasuavida/constant/Constants.dart';
import 'package:edasuavida/constant/dimen.dart';
import 'package:edasuavida/util/MaskedTextController.dart';
import 'package:edasuavida/util/StringUtils.dart';
import 'package:edasuavida/widget/CustomAppBarView.dart';
import 'package:edasuavida/widget/CustomTextFieldView.dart';
import 'package:edasuavida/widget/HeaderTitleView.dart';
import 'package:edasuavida/widget/RoundedButton.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SignUpUserView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SignUpUserStateView();
  }
}

class SignUpUserStateView extends State<SignUpUserView> {
  final TextEditingController completeNameController = TextEditingController();
  final TextEditingController phoneController =
      MaskedTextController(mask: Constants.phone_mask);
  final TextEditingController emailController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  final int phoneLength = 15;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBarView(() {
        Navigator.pop(context);
      }),
      bottomNavigationBar: Container(
          padding: ProjectDimens.default_container_insets,
          child: RoundedButton(Constants.next, onNextClicked)),
      body: SingleChildScrollView(
          child: Container(
              padding: EdgeInsets.only(left: 20, right: 20),
              child: Column(
                children: <Widget>[
                  HeaderTitleView(Constants.sign_up,
                      Constants.fill_fields_below_to_continue),
                  Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        CustomTextFieldView(completeNameController, (value) {
                          return isValidCompleteName(value);
                        }, hintText: Constants.complete_name),
                        CustomTextFieldView(phoneController, (value) {
                          return isValidPhone(value);
                        },
                            inputType: TextInputType.number,
                            labelText: Constants.phone_mask,
                            hintText: Constants.phone),
                        CustomTextFieldView(emailController, (value) {
                          return isValidEmail(value);
                        },
                            inputType: TextInputType.emailAddress,
                            inputAction: TextInputAction.done,
                            hintText: Constants.email),
                      ],
                    ),
                  )
                ],
              ))),
    );
  }

  onNextClicked() {
    Navigator.pushNamed(context, Constants.view_sign_up_campaign);
    if (_formKey.currentState.validate()) {}
  }

  isValidCompleteName(String name) {
    if (name.isEmpty) {
      return Constants.type_your_name;
    }
    if (name.length < 3) {
      return Constants.type_your_name_correctly;
    }
    return null;
  }

  isValidPhone(String phone) {
    if (phone.isEmpty) {
      return Constants.type_your_phone;
    }
    if (phone.length < phoneLength) {
      return Constants.type_your_phone_correctly;
    }
    return null;
  }

  isValidEmail(String email) {
    if (email.isEmpty) {
      return Constants.type_your_email;
    }

    if (!StringUtils.isValidEmail(email)) {
      return Constants.type_your_email_correctly;
    }
    return null;
  }
}
