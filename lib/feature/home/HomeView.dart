import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeView extends StatelessWidget {
  bool selected = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Jãozinho Fofo'),
        ),
        body: BodySnackBar());
  }
}

class BodySnackBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            CarouselSlider(
              height: 230.0,
              autoPlay: true,
              autoPlayInterval: Duration(seconds: 3),
              autoPlayAnimationDuration: Duration(milliseconds: 800),
              autoPlayCurve: Curves.fastOutSlowIn,
              enlargeCenterPage: true,
              scrollDirection: Axis.horizontal,
              initialPage: 0,
              pauseAutoPlayOnTouch: Duration(seconds: 10),
              enableInfiniteScroll: true,
              viewportFraction: 0.8,
              items: [1, 2, 3, 4, 5].map((i) {
                return Builder(
                  builder: (BuildContext context) {
                    return MyStatefulWidget(
                      key: ValueKey(i.toString()),
                    );
                  },
                );
              }).toList(),
            ),
            OutlineButton(
              child: Text("Botão"),
              borderSide: BorderSide(color: Colors.blue),
              onPressed: () {
                final snackBar = SnackBar(
                  content: Text("oi oi oi"),
                  backgroundColor: Colors.green,
                  duration: Duration(seconds: 2),
                );
                Scaffold.of(context).showSnackBar(snackBar);
              },
            ),
          ]),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  MyStatefulWidget({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  bool selected = false;

  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selected = !selected;
        });
      },
      child: Center(
        child: AnimatedPhysicalModel(
          duration: const Duration(milliseconds: 1000),
          curve: Curves.fastOutSlowIn,
          shape: BoxShape.rectangle,
          shadowColor: Colors.black,
          color: Colors.transparent,
          borderRadius: !selected
              ? new BorderRadius.all(const Radius.circular(15.0))
              : new BorderRadius.all(const Radius.circular(8.0)),
          elevation: selected ? 2.0 : 6.0,
          child: AnimatedContainer(
            width: selected ? 300.0 : 200.0,
            height: selected ? 130.0 : 200.0,
            decoration: new BoxDecoration(
              color: selected ? Colors.yellow : Colors.greenAccent,
              borderRadius: !selected
                  ? new BorderRadius.all(const Radius.circular(15.0))
                  : new BorderRadius.all(const Radius.circular(8.0)),
            ),
            alignment:
                selected ? Alignment.center : AlignmentDirectional.topCenter,
            duration: Duration(seconds: 1),
            curve: Curves.fastOutSlowIn,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                FlutterLogo(size: 75),
                AnimatedDefaultTextStyle(
                  duration: Duration(seconds: 1),
                  style: TextStyle(
                      fontSize: selected ? 15 : 10,
                      color: selected ? Colors.black87 : Colors.white),
                  textAlign: TextAlign.center,
                  child: Text(
                    'Matheuzinho crossfiteiro',
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
