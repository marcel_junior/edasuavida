import 'package:edasuavida/constant/Constants.dart';
import 'package:edasuavida/widget/CustomAppBarView.dart';
import 'package:edasuavida/widget/LoadingView.dart';
import 'package:edasuavida/widget/WarningView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

abstract class BaseLoadedViewHelper {

  ScreenStatus status = ScreenStatus.LOADING;
  bool withToolbar = true;

  Widget getCurrentWidget() {
    switch (status) {
      case ScreenStatus.LOADING:
        return _loadingWidget();
        break;
      case ScreenStatus.ERROR:
        return _errorWidget();
        break;
      case ScreenStatus.EMPTY:
        return _emptyWidget();
        break;
      case ScreenStatus.LOADED:
        return onLoaded();
        break;
    }
    return Container(child: Text('Failed in load view'));
  }

  Widget _errorWidget() {
    return Scaffold(
        appBar: withToolbar ? CustomAppBarView(onBackPressed) : null,
        body: WarningView(
            icon: Constants.lottie_error,
            title: Constants.campaign_list_error_title,
            subtitle: Constants.campaign_list_error_subtitle,
            actionText: Constants.campaign_list_error_action,
            onActionTap: () => onServerErrorRetryClicked()));
  }

  Widget _emptyWidget() {
    return Scaffold(
        appBar: withToolbar ? CustomAppBarView(onBackPressed) : null,
        body: WarningView(
          icon: Constants.lottie_empty,
          title: Constants.campaign_list_empty_title,
          subtitle: Constants.campaign_list_empty_subtitle,
          actionText: Constants.campaign_list_empty_action,
          onActionTap: () => onEmptyViewRetryClicked(),
        ));
  }

  Widget _loadingWidget() {
    return Scaffold(
      appBar: withToolbar ? CustomAppBarView(onBackPressed) : null,
      body: LoadingView(),
    );
  }

  Widget onLoaded();

  onServerErrorRetryClicked();

  onEmptyViewRetryClicked();

  onBackPressed();
}

enum ScreenStatus { LOADING, ERROR, EMPTY, LOADED }
