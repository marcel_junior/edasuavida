


import 'package:edasuavida/feature/BaseLoadedView.dart';
import 'package:flutter/cupertino.dart';


abstract class BaseLoadedStatefulViewState<T extends StatefulWidget> extends State<T> with BaseLoadedViewHelper {

  @override
  Widget build(BuildContext context) {
    return getCurrentWidget();
  }

  onLoadingWrapperCallback() {
    setState(() {
      status = ScreenStatus.LOADING;
    });
  }

  onErrorWrapperCallback(Exception exception) {
    setState(() {
      status = ScreenStatus.ERROR;
    });
  }

  onLoadedWrapperCallback(Function onLoaded) {
    setState(() {
      status = ScreenStatus.LOADED;
      onLoaded();
    });
  }

  @override
  onBackPressed() {
    Navigator.pop(context);
  }


}
