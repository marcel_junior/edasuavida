import 'package:edasuavida/constant/Constants.dart';
import 'package:edasuavida/constant/color.dart';
import 'package:edasuavida/constant/dimen.dart';
import 'package:edasuavida/constant/style.dart';
import 'package:edasuavida/constant/theme.dart';
import 'package:edasuavida/feature/campaign/CampaignContentView.dart';
import 'package:edasuavida/widget/WarningView.dart';
import 'package:flutter/material.dart';

class CampaignSearchDelegate extends SearchDelegate {
  CampaignSearchDelegate()
      : super(searchFieldLabel: Constants.search_a_campaign);

  Widget _currentWidget;
  String _currentQuery;

  @override
  ThemeData appBarTheme(BuildContext context) {
    return ThemeData(
        inputDecorationTheme: InputDecorationTheme(
            hintStyle: RomanStyle(
                fontSize: ProjectDimens.font_size_medium,
                color: ProjectColors.secondary_4)),
        primaryColor: ProjectColors.background_1,
        textTheme: ProjectThemes.lightText);
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Image.asset(Constants.icon_close),
        onPressed: () {
          query = '';
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Image.asset(Constants.image_back_button),
      onPressed: () {
        close(context, null);
      },
    );
  }

  //TODO Handle search result and layout
  @override
  Widget buildResults(BuildContext context) {
    if (query.length < 3) {
      return _currentWidget ?? WarningView(
        icon: Constants.lottie_empty,
        title: Constants.campaign_search_error_size_title,
        subtitle: Constants.campaign_search_error_size_subtitle,
      );
    }
    if (_currentQuery != query) {
      _currentQuery = query;
      _currentWidget = new CampaignContentView(query: query);
    }
    return _currentWidget;
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return _currentWidget ??
        WarningView(
          icon: Constants.lottie_empty,
          title: Constants.campaign_search_title,
          subtitle: Constants.campaign_search_subtitle,
        );
  }
}
