import 'package:edasuavida/data/ResquestCallbackWrapper.dart';
import 'package:edasuavida/data/model/ProposalResult.dart';
import 'package:edasuavida/data/network/projectApi.dart';

class CampaignController {

  getTags(onLoading(), onSuccess(List<String> tags), onError(Exception error)) {
    CallbackRequestWrapper<List<String>> _observer = wrapRequest();
    _observer.onLoading(onLoading).onError(onError).onSuccess(onSuccess);
    _observer.run((callback) => ProjectApi.instance.getTagsList(callback));
  }

}
