import 'dart:io';

import 'package:edasuavida/constant/Constants.dart';
import 'package:edasuavida/constant/color.dart';
import 'package:edasuavida/constant/dimen.dart';
import 'package:edasuavida/constant/style.dart';
import 'package:edasuavida/data/model/Proposal.dart';
import 'package:edasuavida/feature/contact/ContactView.dart';
import 'package:edasuavida/feature/login/LoginController.dart';
import 'package:edasuavida/feature/login/LoginView.dart';
import 'package:edasuavida/util/MessageUtils.dart';
import 'package:edasuavida/widget/CustomAppBarView.dart';
import 'package:edasuavida/widget/IconTextButton.dart';
import 'package:edasuavida/widget/ImageView.dart';
import 'package:edasuavida/widget/RoundedButton.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';

class CampaignDetailView extends StatefulWidget {
  final Proposal proposal;

  CampaignDetailView({this.proposal});

  @override
  _CampaignDetailState createState() {
    return _CampaignDetailState();
  }
}

class _CampaignDetailState extends State<CampaignDetailView> {
  LoginController _controller = LoginController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBarView(() => Navigator.pop(context), shareAction: true,
          onSharePressed: () {
        onSharePressed(widget.proposal.title);
      }),
      bottomNavigationBar: Material(
        shadowColor: ProjectColors.shadow_1,
        color: ProjectColors.background_1,
        elevation: 32,
        child: Container(
          padding: EdgeInsets.symmetric(
              vertical: Platform.isIOS ? 30 : ProjectDimens.margin_small,
              horizontal: ProjectDimens.margin_medium),
          child: RoundedButton(Constants.campaign_detail_help, onHelpClicked),
        ),
      ),
      body: Container(
        color: ProjectColors.background_1,
        height: double.infinity,
        child: SingleChildScrollView(
          padding: EdgeInsets.only(top: ProjectDimens.margin_small),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              _buildImage(),
              _buildTitle(),
              _buildSubtitle(),
              _buildReportButton()
//                _buildSiteButton()
            ],
          ),
        ),
      ),
    );
  }

  _launchURL() async {
//    const url = 'https://flutter.io';
//    if (await canLaunch(url)) {
//      await launch(url, forceWebView: true);
//    } else {
//      throw 'Could not launch $url';
//    }
  }

  Widget _buildImage() => Container(
        width: double.infinity,
        height: 200,
        child: Stack(
          alignment: const Alignment(0.9, 0.9),
          children: <Widget>[
            Hero(
              tag: "img_${widget.proposal.proposalId}",
              transitionOnUserGestures: true,
              child: Container(
                  decoration: BoxDecoration(
                    boxShadow: [
                        BoxShadow(
                          color: ProjectColors.secondary_6.withOpacity(0.7),
                          spreadRadius: 1,
                          blurRadius: 0,
                          offset: Offset(0, 0.75), // changes position of shadow
                        ),
                    ]
                  ),
                  child: ImageView(
                  radius: BorderRadius.all(Radius.circular(0)),
                  imgUrl: widget.proposal.imageURL == null
                      ? ''
                      : widget.proposal.imageURL)),
            ),
            Opacity(
              opacity: widget.proposal.images.length > 0 ? 1.0 : 0.0,
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 4, horizontal: 10),
                decoration: BoxDecoration(
                  color: ProjectColors.background_2,
                  borderRadius: BorderRadius.all(Radius.circular(24)),
                ),
                child: Text("1 / ${widget.proposal.images.length}",
                    style: TextStyle(color: Colors.white)),
              ),
            ),
          ],
        ),
      );

  Widget _buildTitle() => Material(
        type: MaterialType.transparency,
        child: Container(
          width: double.infinity,
          margin: EdgeInsets.only(top: 32, left: 20, right: 20, bottom: 16),
          child: Hero(
              tag: "title_${widget.proposal.proposalId}",
              transitionOnUserGestures: true,
              child: Text(widget.proposal.title, style: BlackStyle.large)),
        ),
      );

  Widget _buildSubtitle() => Container(
        margin: EdgeInsets.symmetric(horizontal: 20),
        alignment: Alignment.bottomLeft,
        child: Text(widget.proposal.description, style: RomanStyle.medium),
      );

  Widget _buildReportButton() => Padding(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
    children: <Widget>[
      Padding(
          padding: EdgeInsets.only(bottom: 28, top: 28),
          child: Divider(color: ProjectColors.secondary_3)),
          IconTextButton(iconPath: Constants.icon_report, title: Constants.report_campaign, onClick: () {
            MessageUtils.sendEmail(Constants.report_email, Constants.report_campaign_email_title.replaceAll('{name}', widget.proposal.title));
          })
    ],
  ));

//  Widget _buildSiteButton() => Row(
//        children: <Widget>[
//          Container(
//            margin: EdgeInsets.symmetric(horizontal: 20, vertical: 24),
//            decoration: BoxDecoration(
//              border: Border.all(
//                  color: ProjectColors.secondary_3,
//                  width: 1.0,
//                  style: BorderStyle.solid),
//              borderRadius: BorderRadius.all(Radius.circular(8)),
//            ),
//            child: Material(
//              borderRadius: BorderRadius.all(Radius.circular(8)),
//              child: InkWell(
//                onTap: _launchURL,
//                borderRadius: BorderRadius.all(Radius.circular(8)),
//                highlightColor: ProjectColors.highlight_1,
//                splashColor: ProjectColors.secondary_6,
//                child: Padding(
//                  padding: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
//                  child: Text(
//                    Constants.campaign_detail_fo_to_site,
//                    style: RomanStyle.medium,
//                  ),
//                ),
//              ),
//            ),
//          ),
//        ],
//      );

  onHelpClicked() {
    _controller.verifyLogin((firebaseUser) {
      callView(ContactView(proposal: widget.proposal));
    }, () {
      callView(LoginView(proposal: widget.proposal));
    });
  }

  callView(final Widget widget) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => widget));
  }

  onSharePressed(campaignName) {
    Share.share(
        Constants.share_message_detail.replaceAll('{name}', campaignName));
  }
}
