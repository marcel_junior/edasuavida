import 'package:edasuavida/constant/Constants.dart';
import 'package:edasuavida/constant/color.dart';
import 'package:edasuavida/constant/style.dart';
import 'package:edasuavida/data/model/Proposal.dart';
import 'package:edasuavida/data/network/projectApi.dart';
import 'package:edasuavida/feature/campaign/CampaignDetailView.dart';
import 'package:edasuavida/feature/campaign/CampaignSearchDelegate.dart';
import 'package:edasuavida/widget/CampaignFiltersView.dart';
import 'package:edasuavida/widget/CampaignItemView.dart';
import 'package:edasuavida/widget/HeaderTitleView.dart';
import 'package:edasuavida/widget/WarningView.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pagewise/flutter_pagewise.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:shimmer/shimmer.dart';

const _PAGE_SIZE = 10;

// ignore: must_be_immutable
class CampaignContentView extends StatefulWidget {
  final String query;
  List<String> _typeFilterList;

  PagewiseLoadController<Proposal> controller;

  CampaignContentView({this.query}) {
    getCampaigns(query: query);
  }

  getCampaigns({query}) async {
    controller = PagewiseLoadController<Proposal>(
        pageSize: _PAGE_SIZE,
        pageFuture: (page) async {
          var result = await ProjectApi.instance.getCampaignList(
              page, _PAGE_SIZE,
              searchQuery: query, filterTypeList: _typeFilterList);
          return result.result;
        });
  }

  @override
  _CampaignContentState createState() {
    return _CampaignContentState();
  }
}

class _CampaignContentState extends State<CampaignContentView> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return _listWidget(context);
  }

  void _onItemClick(BuildContext context, Proposal proposal) {
    Navigator.push(
      context,
      materialRoute(context, proposal),
    );
  }

  Widget _loadingWidget(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: ProjectColors.gray_1,
      highlightColor: Colors.grey[300],
      enabled: true,
      child: Column(children: <Widget>[
        CampaignItemLoadingView(),
        CampaignItemLoadingView(),
        CampaignItemLoadingView(),
        CampaignItemLoadingView(),
        CampaignItemLoadingView(),
      ]),
    );
  }

  Widget _errorWidget(BuildContext context) {
    return WarningView(
      icon: Constants.lottie_error,
      title: Constants.campaign_list_error_title,
      subtitle: Constants.campaign_list_error_subtitle,
      actionText: Constants.campaign_list_error_action,
      onActionTap: () => widget.controller.retry(),
    );
  }

  Widget _emptyWidget(BuildContext context) {
    return WarningView(
      icon: Constants.lottie_empty,
      title: Constants.campaign_list_empty_title,
      subtitle: Constants.campaign_list_empty_subtitle,
      actionText: Constants.campaign_list_empty_action,
      onActionTap: () => widget.controller.reset(),
    );
  }

  Widget _listWidget(BuildContext context) {
    var children = <Widget>[];
    if (widget.query == null) {
      children.add(_getSliverAppBar());
    }
    children.add(_getCampaignSliverList());
    return Scaffold(
        key: _scaffoldKey,
        body: Container(
            color: ProjectColors.background_1,
            child: CustomScrollView(
              slivers: children,
            )));
  }

  _getCampaignSliverList() {
    return AnimationLimiter(
        child: PagewiseSliverList(
      itemBuilder: _buildItem,
      pageLoadController: widget.controller,
      showRetry: true,
      loadingBuilder: _buildLoadingItem,
      retryBuilder: _buildRetryItem,
      noItemsFoundBuilder: _buildEmptyItem,
    ));
  }

  _getSliverAppBar() {
    return SliverAppBar(
      actions: <Widget>[
        IconButton(
          icon: Image.asset(Constants.icon_search),
          onPressed: _handleSearchActionClicked,
        )
      ],
      leading: IconButton(
          icon: Image.asset(Constants.image_back_button),
          onPressed: () {
            Navigator.pop(context);
          }),
      floating: true,
      pinned: true,
      backgroundColor: ProjectColors.background_1,
      expandedHeight: 230,
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(70),
        child: Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(bottom: 10, top: 10, left: 20),
            /*TODO Replace to horizontal scroll when to add other filters*/
            child: CampaignFiltersView((filterList) {
              _onTypeFilterApplied(filterList);
            }, _scaffoldKey)),
      ),
      flexibleSpace: FlexibleSpaceBar(
        collapseMode: CollapseMode.parallax,
        background: Container(
            padding: EdgeInsets.only(top: 80, left: 20, right: 20),
            child: HeaderTitleView(Constants.how_you_can_help_title,
                Constants.how_you_can_help_description)),
      ),
    );
  }

  Route<Object> materialRoute(BuildContext context, Proposal proposal) {
    var _pageRoute = MaterialPageRoute(
        builder: (context) => CampaignDetailView(proposal: proposal));
    return _pageRoute;
  }

  Widget _buildItem(BuildContext context, Proposal proposal, int index) {
    return AnimationConfiguration.staggeredList(
        position: index,
        duration: const Duration(milliseconds: 600),
        child: SlideAnimation(
          verticalOffset: 300,
          child: FadeInAnimation(
            child: CampaignItemView(
                proposal: proposal,
                onItemTap: (proposal) => _onItemClick(context, proposal)),
          ),
        ));
  }

  Widget _buildLoadingItem(BuildContext context) {
    return widget.controller.loadedItems == null ||
            widget.controller.loadedItems.isEmpty
        ? _loadingWidget(context)
        : CircularProgressIndicator();
  }

  Widget _buildRetryItem(BuildContext context, Function function) {
    return widget.controller.loadedItems == null ||
            widget.controller.loadedItems.isEmpty
        ? _errorWidget(context)
        : Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: function,
              child: Container(
                padding: EdgeInsets.all(16),
                alignment: Alignment.center,
                child: Text(
                  Constants.campaign_list_error_list_action,
                  textAlign: TextAlign.center,
                  style: RomanStyle.medium,
                ),
              ),
            ),
          );
  }

  Widget _buildEmptyItem(BuildContext context) {
    return widget.controller.loadedItems == null ||
            widget.controller.loadedItems.isEmpty
        ? _emptyWidget(context)
        : Text(Constants.campaign_list_end_pagination, style: RomanStyle.small);
  }

  _handleSearchActionClicked() {
    showSearch(
      context: context,
      delegate: CampaignSearchDelegate(),
    );
  }

  _onTypeFilterApplied(filterList) {
    widget._typeFilterList = filterList;
    widget.controller.reset();
//    widget.controller = PagewiseLoadController<Proposal>(
//        pageSize: _PAGE_SIZE,
//        pageFuture: (page) async {
//          var result = await ProjectApi.instance.getCampaignListBacate(page, _PAGE_SIZE, filterTypeList: filterList);
//          return result.result;
//        });
  }
}
