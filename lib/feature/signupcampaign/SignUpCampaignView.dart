import 'package:edasuavida/constant/Constants.dart';
import 'package:edasuavida/constant/dimen.dart';
import 'package:edasuavida/widget/CustomAppBarView.dart';
import 'package:edasuavida/widget/CustomDropdownView.dart';
import 'package:edasuavida/widget/CustomTextFieldView.dart';
import 'package:edasuavida/widget/HeaderTitleView.dart';
import 'package:edasuavida/widget/RoundedButton.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SignUpCampaignView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SignUpCampaignViewState();
  }
}

class SignUpCampaignViewState extends State<SignUpCampaignView> {
  final TextEditingController completeNameController = TextEditingController();
  final TextEditingController siteController = TextEditingController();
  final TextEditingController descriptionTextController =
      TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: Container(
            padding: ProjectDimens.default_container_insets,
            child: RoundedButton(Constants.next, () {})),
        appBar: CustomAppBarView(() {
          Navigator.pop(context);
        }),
        body: SingleChildScrollView(child: Form(
            child: Container(
          padding: ProjectDimens.default_container_insets,
          child: Column(
            children: <Widget>[
              HeaderTitleView(Constants.sign_up_campaign,
                  Constants.fill_fields_below_to_continue),
              CustomDropdownView(
                  Constants.type, [Constants.label_pf, Constants.label_pj]),
              CustomTextFieldView(completeNameController, (value) {
                return null;
              }, hintText: Constants.complete_name),
              CustomDropdownView(Constants.category, [Constants.label_commerce]),
              CustomTextFieldView(siteController, (value) {
                return null;
              }, hintText: Constants.site),
              CustomTextFieldView(descriptionTextController, (value) {
                return null;
              }, hintText: Constants.description)
            ],
          ),
        ))));
  }
}
