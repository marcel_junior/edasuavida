import 'package:edasuavida/constant/Constants.dart';
import 'package:edasuavida/data/storage/projectDatabase.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intro_slider/slide_object.dart';

import '../../constant/style.dart';
import '../../widget/IntroSlider.dart';

class IntroView extends StatefulWidget {
  @override
  _IntroViewState createState() => _IntroViewState();
}

class _IntroViewState extends State<IntroView> {
  List<Slide> slides = new List();

  @override
  void initState() {
    slides.add(
      new Slide(
        title: Constants.intro_title_1,
        description: Constants.intro_description_1,
        pathImage: Constants.intro_image_1,
        styleTitle: BlackStyle.large,
        styleDescription: RomanStyle.normal,
        backgroundColor: Colors.white,
      ),
    );
    slides.add(
      new Slide(
        title: Constants.intro_title_2,
        description: Constants.intro_description_2,
        pathImage: Constants.intro_image_2,
        styleTitle: BlackStyle.large,
        styleDescription: RomanStyle.normal,
        backgroundColor: Colors.white,
      ),
    );
    slides.add(
      new Slide(
        title: Constants.intro_title_3,
        description: Constants.intro_description_3,
        pathImage: Constants.intro_image_3,
        styleTitle: BlackStyle.large,
        styleDescription: RomanStyle.normal,
        backgroundColor: Colors.white,
      ),
    );
  }

  void onDonePress() async {
    await ProjectDatabase.burnOnBoarding();
    Navigator.pushReplacementNamed(context, Constants.view_choose_a_way);
  }

  @override
  Widget build(BuildContext context) {
    return new IntroSlider(
        slides: this.slides,
        onDonePress: this.onDonePress,
        nameDoneBtn: Constants.done,
        nameNextBtn: Constants.next,
        styleNameDoneBtn: HeavyStyle.medium,
        highlightColorDoneBtn: Colors.redAccent,
        isShowSkipBtn: false);
  }
}
