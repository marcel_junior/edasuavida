import 'package:edasuavida/constant/Constants.dart';
import 'package:edasuavida/constant/color.dart';
import 'package:edasuavida/constant/dimen.dart';
import 'package:edasuavida/constant/style.dart';
import 'package:edasuavida/widget/CheckBoxView.dart';
import 'package:edasuavida/widget/CustomAppBarView.dart';
import 'package:edasuavida/widget/RoundedButton.dart';
import 'package:flutter/material.dart';

class TermsView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _TermsView();
  }
}

class _TermsView extends State<TermsView> {
  bool _isUseTermsAndPrivacyPolicyChecked = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBarView(() {
        Navigator.pop(context);
      }),
      bottomNavigationBar: PhysicalModel(
        shape: BoxShape.rectangle,
        color: Colors.transparent,
        elevation: 12,
          child: Container(
            color: ProjectColors.background_1,
            padding: EdgeInsets.only(
                left: ProjectDimens.margin_medium,
                right: ProjectDimens.margin_medium,
                bottom: ProjectDimens.margin_medium,
                top: ProjectDimens.margin_medium),
            margin: EdgeInsets.only(
              top: 1,
            ),
            child: RoundedButton(Constants.got_it, onAcceptClicked),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          color: ProjectColors.background_1,
          padding: EdgeInsets.only(
            left: ProjectDimens.margin_medium,
            right: ProjectDimens.margin_medium,
          ),
          child: ConstrainedBox(
            constraints: BoxConstraints(
              minHeight: double.minPositive,
            ),
            child: IntrinsicHeight(
              child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: ProjectDimens.margin_medium),
                    width: double.infinity,
                    child: Text(Constants.terms_title, style: BlackStyle.large),
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Container(
                    margin:
                        EdgeInsets.only(bottom: ProjectDimens.margin_medium),
                    width: double.infinity,
                    child: Text(
                      Constants.terms_text,
                      style: RomanStyle.normal,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  onAcceptClicked() {
    Navigator.pop(context);
  }
}
