import 'package:edasuavida/constant/Constants.dart';
import 'package:edasuavida/constant/dimen.dart';
import 'package:edasuavida/widget/CustomAppBarView.dart';
import 'package:edasuavida/widget/CustomTextFieldView.dart';
import 'package:edasuavida/widget/HeaderTitleView.dart';
import 'package:edasuavida/widget/RoundedButton.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CreatePasswordView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return CreatePasswordViewState();
  }
}

class CreatePasswordViewState extends State<CreatePasswordView> {
  final TextEditingController _passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  final int passwordLength = 6;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
          padding: ProjectDimens.default_container_insets,
          child: RoundedButton(Constants.next, onNextClicked)),
      appBar: CustomAppBarView(() {
        Navigator.pop(context);
      }),
      body: Container(
          padding: ProjectDimens.default_container_insets,
          child: Column(children: <Widget>[
            HeaderTitleView(
                Constants.sign_up, Constants.create_a_password_to_enter_in_app),
            Form(
                key: _formKey,
                child: Column(children: <Widget>[
                  CustomTextFieldView(_passwordController, (value) {
                    return isValidPassword(value);
                  }, passwordType: true, maxLength: passwordLength)
                ]))
          ])),
    );
  }

  onNextClicked() {
    if (_formKey.currentState.validate()) {}
  }

  isValidPassword(String value) {
    if (value.isEmpty) {
      return Constants.type_your_password;
    }
    if (value.trim().length < passwordLength) {
      return getPasswordErrorMessage(passwordLength);
    }
    return null;
  }

  getPasswordErrorMessage(int length) {
    return Constants.minimum_password_length
        .replaceAll('{value}', length.toString());
  }
}
