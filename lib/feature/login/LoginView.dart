import 'dart:io';

import 'package:apple_sign_in/apple_sign_in_button.dart';
import 'package:device_info/device_info.dart';
import 'package:edasuavida/constant/Constants.dart';
import 'package:edasuavida/constant/color.dart';
import 'package:edasuavida/constant/style.dart';
import 'package:edasuavida/data/model/Proposal.dart';
import 'package:edasuavida/feature/contact/ContactView.dart';
import 'package:edasuavida/feature/login/LoginController.dart';
import 'package:edasuavida/feature/terms/TermsView.dart';
import 'package:edasuavida/widget/CheckBoxView.dart';
import 'package:edasuavida/widget/CustomAppBarView.dart';
import 'package:edasuavida/widget/HeaderTitleView.dart';
import 'package:edasuavida/widget/LabeledDividerView.dart';
import 'package:edasuavida/widget/LabeledTextButton.dart';
import 'package:edasuavida/widget/LoadingView.dart';
import 'package:edasuavida/widget/SmoothImageButton.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class LoginView extends StatefulWidget {
  final Proposal proposal;

  LoginView({this.proposal});

  @override
  State<StatefulWidget> createState() {
    return LoginViewState();
  }
}

class LoginViewState extends State<LoginView> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  bool _isUseTermsAndPrivacyPolicyChecked = true;

  bool _isLoading = false;

  bool appleSignIn = false;

  LoginController controller = LoginController();

  @override
  void initState() {
    super.initState();
    if (Platform.isIOS) {
      DeviceInfoPlugin().iosInfo.then((iosInfo) {
        var version = iosInfo.systemVersion;
        if (version.contains('13')) {
          setState(() {
            appleSignIn = true;
          });
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: CustomAppBarView(() {
        Navigator.pop(context);
      }),
      body: _isLoading
          ? LoadingView()
          : Container(
              padding: EdgeInsets.only(left: 20, right: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      HeaderTitleView(Constants.enter,
                          Constants.you_need_do_login_to_enter),
                      Container(
                          padding: EdgeInsets.only(top: 50),
                          child: SmoothImageButton(
                              Constants.image_google,
                              Constants.enter_with_google_account,
                              onGoogleAccountClicked)),
                      Container(
                          padding: EdgeInsets.only(top: 20),
                          child: SmoothImageButton(
                              Constants.image_facebook,
                              Constants.enter_with_facebook,
                              onFacebookClicked)),
                      appleSignIn? Container(
                          padding: EdgeInsets.only(top: 20),
                          child: AppleSignInButton(
                            style: ButtonStyle.whiteOutline,
                            cornerRadius: 30,
                            type: ButtonType.continueButton,
                            onPressed: onAppleButtonClicked,
                          )) : Container()
//                  SmoothImageButton(Constants.image_instagram,
//                      Constants.enter_with_google_account, onInstagramClicked),
//                      Container(
//                          padding: EdgeInsets.only(top: 64, bottom: 44),
//                          child: LabeledDividerView(Constants.or)),
//                      LabeledTextButton(
//                          Constants.enter_in_another_way, onInAnotherWayClicked)
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 40),
                    child: CheckBoxView(
                        RichText(
                          text: TextSpan(
                              style:
                                  RomanStyle(color: ProjectColors.secondary_1),
                              children: <TextSpan>[
                                TextSpan(text: Constants.i_agree_with_the),
                                TextSpan(
                                    recognizer: new TapGestureRecognizer()
                                      ..onTap = onUseTermsAndPrivacyClicked,
                                    text: Constants.use_terms,
                                    style: RomanStyle(
                                        textDecoration:
                                            TextDecoration.underline,
                                        color: ProjectColors.secondary_1)),
                                TextSpan(text: Constants.and),
                                TextSpan(
                                    recognizer: new TapGestureRecognizer()
                                      ..onTap = onUseTermsAndPrivacyClicked,
                                    text: Constants.privacy_policy,
                                    style: RomanStyle(
                                        textDecoration:
                                            TextDecoration.underline,
                                        color: ProjectColors.secondary_1)),
                              ]),
                        ),
                        _isUseTermsAndPrivacyPolicyChecked, (isChecked) {
                      setState(() {
                        _isUseTermsAndPrivacyPolicyChecked = isChecked;
                      });
                    }),
                  ),
                ],
              ),
            ),
    );
  }

  onGoogleAccountClicked() {
    _doSocialLogin(LoginType.GoogleLogin);
  }

  onFacebookClicked() {
    _doSocialLogin(LoginType.FacebookLogin);
  }

  onAppleButtonClicked() {
    _doSocialLogin(LoginType.AppleLogin);
  }

  onInAnotherWayClicked() {
    Navigator.pushNamed(context, Constants.view_sign_up_user);
  }

  _doSocialLogin(LoginType loginType) {
    controller.doSocialLogin(loginType, _isUseTermsAndPrivacyPolicyChecked, (FirebaseUser firebaseUser) {
      doLogin();
      return;
    }, (error) {
      _handleError(error);
    });
  }

  doLogin() {
    controller.addOrUpdateServerUser(() {
      _showLoading();
    }, (user) {
      getUpdatedUser();
    }, (error) {
      _hideLoading();
      _handleError(error);
    });
  }

  getUpdatedUser() {
    controller.getServerUser(() {}, (user) {
      _handleUserAndCallHome(user);
    }, (error) {
      _hideLoading();
      _handleError(error);
    });
  }

  _handleUserAndCallHome(user) {
    controller.saveUserInPreferences(user);
    callContactView();
  }

  callContactView() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => ContactView(proposal: widget.proposal)));
  }

  _hideLoading() {
    setState(() {
      _isLoading = false;
    });
  }

  _showLoading() {
    setState(() {
      _isLoading = true;
    });
  }

  _handleError(FormatException error) {
    showSnackBar(error.message);
  }

  onUseTermsAndPrivacyClicked() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => TermsView()));
  }

  showSnackBar(String message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(message)));
  }
}
