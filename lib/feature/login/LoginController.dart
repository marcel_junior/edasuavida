import 'package:apple_sign_in/apple_sign_in.dart';
import 'package:edasuavida/constant/Constants.dart';
import 'package:edasuavida/data/ResquestCallbackWrapper.dart';
import 'package:edasuavida/data/model/User.dart';
import 'package:edasuavida/data/network/projectApi.dart';
import 'package:edasuavida/data/storage/projectDatabase.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';

class LoginController {

  final List<String> facebookLoginPermissions = ['email', 'public_profile'];
  final List<String> googleLoginScopes = [
    'email',
  ];

  final firebaseAuth = FirebaseAuth.instance;

  static const String facebookLoginType = 'FacebookLogin';
  static const String googleLoginType = 'GoogleLogin';

  verifyLogin(onLoginVerified(FirebaseUser firebaseUser), onLoginError()) {
    firebaseAuth.currentUser().then((firebaseUser) {
      if (firebaseUser != null) {
        _setToken(firebaseUser);
        onLoginVerified(firebaseUser);
        return;
      }
      onLoginError();
    });
  }

  doSocialLogin(LoginType loginType, bool isUseTermsChecked, Function onLoginSuccess(FirebaseUser firebaseUser), onLoginError(FormatException error)) {
    if(!isUseTermsChecked) return onLoginError(FormatException(Constants.you_need_accept_use_terms));

    switch (loginType) {
      case LoginType.FacebookLogin:
        _doFacebookSignIn(onLoginSuccess, onLoginError);
        break;
      case LoginType.GoogleLogin:
        _doGoogleSignIn(onLoginSuccess, onLoginError);
        break;
      case LoginType.AppleLogin:
        _doAppleSignIn(onLoginSuccess, onLoginError);
        break;
      default:
        print('Type not Recgnized');
        break;
    }
  }

  _doFacebookSignIn(
      onLoginSuccess(FirebaseUser firebaseUser), onLoginError(FormatException error)) async {
    FacebookLogin facebookLogin = FacebookLogin();
    FacebookLoginResult facebookLoginResult =
        await facebookLogin.logIn(facebookLoginPermissions);
    switch (facebookLoginResult.status) {
      case FacebookLoginStatus.loggedIn:
        _onFacebookLoggedIn(facebookLoginResult, onLoginSuccess, onLoginError);
        break;
      case FacebookLoginStatus.cancelledByUser:
        print('Facebook Login cancelled by user');
        break;
      case FacebookLoginStatus.error:
        onLoginError(FormatException(facebookLoginResult.errorMessage));
        break;
    }
  }

  _onFacebookLoggedIn(
      FacebookLoginResult result,
      onLoginSuccess(FirebaseUser firebaseUser),
      onLoginError(FormatException error)) async {
    final facebookToken = result.accessToken.token;
    final AuthCredential authCredential = FacebookAuthProvider.getCredential(accessToken: facebookToken);
    _doFirebaseLogin(authCredential, onLoginSuccess, onLoginError);
  }

  _doGoogleSignIn(
      onLoginSuccess(FirebaseUser firebaseUser), onLoginError(FormatException error)) async {
    try {
      GoogleSignIn googleSignIn = GoogleSignIn(scopes: googleLoginScopes);
      GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
      final googleAuth = await googleSignInAccount.authentication;
      final AuthCredential authCredential = GoogleAuthProvider.getCredential(
          idToken: googleAuth.idToken, accessToken: googleAuth.accessToken);
      _doFirebaseLogin(authCredential, onLoginSuccess, onLoginError);
    } catch (error) {
      onLoginError(FormatException(error.toString()));
    }
  }

  _doAppleSignIn(
      onLoginSuccess(FirebaseUser firebaseUser),
      onLoginError(FormatException error)) async {
    AuthorizationResult result = await AppleSignIn.performRequests([
      AppleIdRequest(requestedScopes: [Scope.email, Scope.fullName])
    ]);

    switch (result.status) {
      case AuthorizationStatus.authorized:
        final AppleIdCredential appleIdCredential = result.credential;
        OAuthProvider oAuthProvider = OAuthProvider(providerId: "apple.com");
        final AuthCredential credential = oAuthProvider.getCredential(
            idToken: String.fromCharCodes(appleIdCredential.identityToken),
            accessToken:
            String.fromCharCodes(appleIdCredential.authorizationCode));
        _doFirebaseLogin(credential, onLoginSuccess, onLoginError);
        break;
      case AuthorizationStatus.error:
        onLoginError(FormatException(result.error.toString()));
        break;
      case AuthorizationStatus.cancelled:
        print("Apple Sign in cancelled by user");
        break;
    }
  }

  _doFirebaseLogin(AuthCredential authCredential,
      onLoginSuccess(FirebaseUser firebaseUser), onLoginError(FormatException error)) async {
    
    await firebaseAuth.signInWithCredential(authCredential).then((authResult) {
      if (authResult.user == null) {
        onLoginError(FormatException('Failed to get Firebase User'));
      } else {
        _setToken(authResult.user);
        onLoginSuccess(authResult.user);
      }
      return authResult.user;
    }).catchError((e) {
      PlatformException exception = e;
      onLoginError(FormatException(exception.message));
    });
  }

  _setToken(FirebaseUser firebaseUser) {
    firebaseUser.getIdToken().then((token) {
      ProjectApi.instance.setToken(token.token);
    });
  }

  addOrUpdateServerUser(onLoading(), onSuccess(result), onError(error)) {
    CallbackRequestWrapper<String> _observer = wrapRequest();
    _observer.onLoading(onLoading).onSuccess(onSuccess).onError(onError);
    _observer.run(ProjectApi.instance.saveUser);
  }

  getServerUser(onLoading(), onSuccess(result), onError(error)) {
    CallbackRequestWrapper<User> _observer = wrapRequest();
    _observer.onLoading(onLoading).onSuccess(onSuccess).onError(onError);
    _observer.run(ProjectApi.instance.getUser);
  }

  saveUserInPreferences(User user) {
    ProjectDatabase.saveUser(user);
  }
}

enum LoginType { FacebookLogin, GoogleLogin, AppleLogin }
