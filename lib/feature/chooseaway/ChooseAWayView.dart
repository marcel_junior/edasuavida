import 'dart:async';
import 'dart:io';

import 'package:edasuavida/constant/dimen.dart';
import 'package:edasuavida/constant/style.dart';
import 'package:edasuavida/data/model/ChooseAWayItem.dart';
import 'package:edasuavida/data/model/Configs.dart';
import 'package:edasuavida/data/storage/projectDatabase.dart';
import 'package:edasuavida/util/DialogUtils.dart';
import 'package:edasuavida/widget/ChooseAWayItemView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../constant/Constants.dart';
import '../../constant/color.dart';
import '../BaseLoadedStatefulView.dart';
import 'ConfigsController.dart';

class ChooseAwayView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ChooseAwayViewState();
  }
}

class ChooseAwayViewState extends BaseLoadedStatefulViewState<ChooseAwayView> {
  Configs _configs;
  ConfigsController controller = ConfigsController();

  @override
  void initState() {
    super.initState();

    _getConfigs();

    ProjectDatabase.isCOVIDMessageBurned().then((isBurned) {
      if (!isBurned) {
        Timer.run(() => _showCOVIDDialog());
      }
    });
  }

  /*Verifing platform to show native dialog in iOS cause AppStore analysis*/
  _showCOVIDDialog() {
    if (Platform.isIOS) {
      _createIOSDialog();
    } else {
      _createAndroidDialog();
    }
  }

  _createIOSDialog() {
    Widget gotItButton = CupertinoDialogAction(
      child: Text(Constants.got_it),
      onPressed: () {
        _onGotItClicked();
      },
    );

    showDialog(
        context: context,
        builder: (BuildContext context) => new CupertinoAlertDialog(
              title: new Text(Constants.covid_dialog_title),
              content: new Text(Constants.covid_dialog_description),
              actions: [
                gotItButton
              ],
            ));
  }

  _createAndroidDialog() {
    DialogUtils.showSingleMessageDialog(context, Constants.covid_dialog_title,
        Constants.covid_dialog_description, Constants.got_it, () {
      _onGotItClicked();
    });
  }

  _onGotItClicked() {
    ProjectDatabase.burnCOVIDMessage().then((value) {
      Navigator.pop(context);
    });
  }

  _getConfigs() {
    controller.getURLForm(onLoadingWrapperCallback, onErrorWrapperCallback,
        (result) {
      onLoadedWrapperCallback(() {
        _configs = result;
      });
    });
  }

  @override
  onEmptyViewRetryClicked() {
    _getConfigs();
  }

  @override
  Widget onLoaded() {
    final List<ChooseAWayItem> items = [
      ChooseAWayItem(
          Constants.image_offer_help,
          Constants.offer_help,
          Constants.i_want_to_help_description,
          Constants.view_campaign_list,
          null,
          true),
      ChooseAWayItem(
          Constants.image_search_help,
          Constants.i_need_help_title,
          Constants.i_need_help_description,
          _configs.askHelpUrl,
          null,
          _configs.askHelpUrl != null)
    ];

    return Scaffold(
        appBar: AppBar(backgroundColor: Colors.transparent, elevation: 0),
        backgroundColor: ProjectColors.background_1,
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                padding: EdgeInsets.only(left: 20, top: 20, bottom: 12),
                child: Text(Constants.what_you_want_to_do,
                    style:
                        BlackStyle(fontSize: ProjectDimens.font_size_large))),
            Expanded(
              child: ListView.separated(
                  padding: EdgeInsets.symmetric(vertical: 20),
                  itemCount: items.length,
                  separatorBuilder: (BuildContext context, int index) =>
                      SizedBox(height: 32),
                  itemBuilder: (BuildContext context, int index) {
                    return ChooseAWayItemView(
                        items[index].image,
                        items[index].title,
                        items[index].description,
                        items[index].flag,
                        items[index].enabled, () {
                      var actionRoute = items[index].actionRoute;
                      if (actionRoute != null) {
                        if (Uri.parse(actionRoute).isAbsolute) {
                          launch(actionRoute);
                        } else {
                          Navigator.pushNamed(context, actionRoute);
                        }
                      }
                    });
                  }),
            ),
          ],
        ));
  }

  @override
  onServerErrorRetryClicked() {
    _getConfigs();
  }
}
