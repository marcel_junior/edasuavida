import 'package:edasuavida/data/ResquestCallbackWrapper.dart';
import 'package:edasuavida/data/model/Configs.dart';
import 'package:edasuavida/data/network/projectApi.dart';

class ConfigsController {
  getURLForm(onLoading(), onError(Exception error), onSuccess(result)) {
    CallbackRequestWrapper<Configs> _observer = wrapRequest();
    _observer.onLoading(onLoading).onError(onError).onSuccess(onSuccess);
    _observer.run((callback) => ProjectApi.instance.getConfigs(callback));
  }
}
