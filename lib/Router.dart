import 'package:edasuavida/feature/campaign/CampaignDetailView.dart';
import 'package:edasuavida/feature/campaign/CampaignListView.dart';
import 'package:edasuavida/feature/chooseaway/ChooseAWayView.dart';
import 'package:edasuavida/feature/contact/ContactView.dart';
import 'package:edasuavida/feature/home/HomeView.dart';
import 'package:edasuavida/feature/intro/IntroView.dart';
import 'package:edasuavida/feature/login/LoginView.dart';
import 'package:edasuavida/feature/password/CreatePasswordView.dart';
import 'package:edasuavida/feature/signupcampaign/SignUpCampaignView.dart';
import 'package:edasuavida/feature/signupuser/SignupUserView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'constant/Constants.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case Constants.view_intro:
        return getRouteWrapper(IntroView());
        break;
      case Constants.view_choose_a_way:
        return getRouteWrapper(ChooseAwayView());
        break;
      case Constants.view_login:
        return getRouteWrapper(LoginView());
        break;
      case Constants.view_campaign_list:
        return getRouteWrapper(CampaignListView());
        break;
      case Constants.view_campaign_detail:
        return getRouteWrapper(CampaignDetailView());
        break;
      case Constants.view_home:
        return getRouteWrapper(HomeView());
      case Constants.view_sign_up_user:
        return getRouteWrapper(SignUpUserView());
      case Constants.view_create_password:
        return getRouteWrapper(CreatePasswordView());
      case Constants.view_sign_up_campaign:
        return getRouteWrapper(SignUpCampaignView());
      case Constants.view_contact:
        return getRouteWrapper(ContactView());
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                      child: Text('No route defined for ${settings.name}')),
                ));
    }
  }

  static getRouteWrapper(Widget widget) {
    return MaterialPageRoute(builder: (_) => widget);
  }
}
